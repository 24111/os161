Overall design idea + code architecture (all these are in code comment but I assume you don't wanna read ~1500 lines of codes)

/*====================== ARCHITECTURE ======================*/
addrspace + fault + vm_core reimplements what dumb_vm did
pagetable is a lookup table for disk locations. Map vaddr - disk index
pageswap is a disk memory bitmap and a toolbox for page swapping IOs
sbrk is sbrk
coremap is highly overloaded. It keeps a pagetable pointer for quicker access and for distinguishing memory owner
It keep a tag for various operational flags. Tag also contain pagetable index for identification and lookup, or kmalloc size in case of a kernel page

/*====================== LOGIC ======================*/
COREMAP (handles kmalloc, kfree, tlbmiss, page eviction, page loading...)
tlb: There's no shooting. Instead, keep an INUSE bit in coremap tag
replacement: DIRTY tag in coremap. All page originally read-only. Kernel can't evict dirty pages, but user can.
	page has to go from init - written - unloaded by another proc - loaded (clean), before kmalloc can take advantage of dirty bit
	A cleaner thread would help with this (unimplemented)
VALID: coremap valid = you have to inform the owner of mem stealing. Ideally look for unowned (not VALID) pages
RESERVED: Act as a lock (read on read is not a hazard) to safeguard a memory location while we do IO

PAGETABLE
by default pagetable access would normally requires holding coremap lock. Exceptions apply, and can be found in code comments
INMEM: signal that we should be scanning coremap for the entry
VALID: If this is set, data on disk is valid. We need to load from disk if entry is not in mem. If not set, means that the page is just initialized and written to
TOWED: In case we evict a dirty page and its owner came in looking for it. We need to do IO to the disk, and the owner cannot load it until it's done
vfs_big_lock will help, but we can't acquire it until we release coremap lock. Which means trouble.

PAGESWAP:
Lock a bit map, pretty much it


/*====================== TESTS ======================*/
DISCLAIMER: These test are collective over some minor changes I made in the kernel
Hopefully, I didn't break anything during bug fixes.
I will also list potential issues. These issues may or may not occur and I may or may not have fixed it
(if I found another bug that caused it to appear less frequent, but doesn't explain that particular issue, etc...)
There's a bug somewhere in memory affecting my load-store and is corrupting my data.
All heavily load-store heavy tests (triple, quint, huge, etc) will fail validation checks. This bug is inconsistent and I can't narrow it down
It might be that I'm not zeroing memory somewhere. It might be the special TOWED case - I can't force a TOWED situation and couldn't test it
I ran out of time and energy debugging this OS (By the time of this writing, the last time I left my room or had a proper meal was 30 hours ago)
In general, all tests pass with 16MB in (reasonable) time. 512kb can take a very long time to finish. Details below

km1: Pass
km2: Pass
km3: Pass, if parameter is not larger than largest chunk of not fragmented memory (~3000 for 512kb last time I checked)
km4: Pass for 16mb
forktest: Pass
multiexec: Pass (There's a bug somewhere that hung it on 512kb occasionally - likely the same corruption bug. Maybe 1/4-5 call?)

sbrktest: Pass all required test. Test 19-21 will pass on 16mb, but very slowly. I didn't have the patience to test it in 512kb
malloctest: Test 2 - 3 is very slow. I would not recommend running them on 16MB - with 160 MB of swap space in total, the system WILL try to give you
all that memory. A failed malloc would give you as much as it can (I got lazy and didn't revoke the disk mem on failure - only proc exit
not a requirement anyway), you will end up with no swap page! This will fail any new processes and halt working until current process exit, 
after which the memory is reclaimed

huge: Pass on 16MB. Takes VERY long on 512kb but did eventually finish - with failed check
matmult: Pass. Very slow on 512kb
triplemat: Pass on 16MB. Very slow on 512kb, and didn't pass.
quintmat: Pass on 16MB. Didn't run on 512kb
sort: Passed. Slow on 512kb
