1. Exit: The thread is removed from its process, and is marked for removal 
(S_ZOMBIE) in thread_switch and the next thread to run is fetched.
Err1: the owner process cannot find the thread exiting to remove: 
panic("Thread (%p) has escaped from its process (%p)\n", t, proc);
Err2: Dead thread walkin:
panic("braaaaaaaiiiiiiiiiiinssssss\n");

Sleep: The thread is marked as S_SLEEP and is placed on the wait channel

	-------------------------------------------------------------------

2. thread_switch handles the switch. It calls switchframe_switch to handle the context switch
(thread_switch itself handles the thread states and choosing the next thread to switch), and
the code for context switching itself is in switch.S 

	-------------------------------------------------------------------

3. S_RUN: currently running.
S_READY: also currently running, but not quite
It's waiting until its turn to run
S_SLEEP: Sleeping on a wait channel. Until it's waken up.
S_ZOMBIE: Finished its job, waiting for its removal.

	-------------------------------------------------------------------

4. Turning interrupt off means the current code will not be interrupted by ISR.
The code will be executed as an atomic operation.
It is required because a timer interrupt to signal thread switch can cause errors if
the current thread operation is not finished.

	-------------------------------------------------------------------

5. It calls to the wait channel containing the sleeping thread to wake it up
The thread is fetched and removed from the list of sleeping threads (or all in the
event of wakeall), and is called to switch to S_READY.
thread_switch add the thread back to run queue.

	-------------------------------------------------------------------

6 + 7 + 8. thread_switch handles thread switching. schedule select the next thread to run (default: round-robin)
thread_yield lay off the current thread (to S_READY). schedule() and thread_yield is called
from hardclock (clock.c) which is called from mainbus_interrupt (lamebus_machdep.c), which is called in
trap handler when the trap cause is determined to be IRQ (mips_trap, trap.c)
Or, in other word: ISR lay off the current running thread and schedule the next thread to run.
thread_switch fetches the first thread in the running queue and run it.
schedule (not implemented) would probably sort the running queue to achieve the desired goal

	-------------------------------------------------------------------

9. wchan_sleep put the current thread to sleep and place it in the specified wait channel
wchan_wakeone signals one thread to wake up.
Semaphores have its own internal wait channel and lock. Threads communicating through semaphores are
put to sleep and signaled to wake up through the semaphore's private channel.

	-------------------------------------------------------------------

10. The wakeup call fetch from the list of sleeping thread. A thread marked as S_SLEEP but not placed
in the channel will not wake up, as it is not present in the list.