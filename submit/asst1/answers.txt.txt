1. __start ()d

gdb:

Remote debugging using unix:.sockets/gdb
__start () at ../../arch/sys161/main/start.S:54
54	   addiu sp, sp, -24


2.
0x8002d6f0 <+0>:	addiu	sp,sp,-24

gdb:
=> 0x8002d6f0 <+0>:	addiu	sp,sp,-24
   0x8002d6f4 <+4>:	sw	ra,20(sp)
   0x8002d6f8 <+8>:	lui	s0,0x8003
   0x8002d6fc <+12>:	addiu	s0,s0,23584
   0x8002d700 <+16>:	move	a1,a0
   0x8002d704 <+20>:	move	a0,s0
   0x8002d708 <+24>:	jal	0x80001d30 <strcpy>
   0x8002d70c <+28>:	nop
   0x8002d710 <+32>:	move	a0,s0
   0x8002d714 <+36>:	jal	0x80001de0 <strlen>
   0x8002d718 <+40>:	nop
   0x8002d71c <+44>:	add	t0,s0,v0
   0x8002d720 <+48>:	addi	t0,t0,1
   0x8002d724 <+52>:	addi	t0,t0,4095
   0x8002d728 <+56>:	li	t1,-4096

3.
Num     Type           Disp Enb Address    What
0       breakpoint     keep y   0x800139e4 in kmain at ../../main/main.c:211
1       breakpoint     keep y   0x80014a0c in menu at ../../main/menu.c:697

4.	The memory map is set up, then the stack frame, the exception handler code 
is copied to the first page of the memory, set up the status register, and finally load the CPU number, GP register.

5.       0x8002d7b4 <+196>:	jal	0x800139d0 <kmain>
gdb:
   0x8002d7ac <+188>:	lui	gp,0x8004
   0x8002d7b0 <+192>:	addiu	gp,gp,-16192
   0x8002d7b4 <+196>:	jal	0x800139d0 <kmain>
   0x8002d7b8 <+200>:	move	a0,s0

6.
Breakpoint 2, boot () at ../../main/main.c:109
109		ram_bootstrap();
(gdb) n

Breakpoint 3, boot () at ../../main/main.c:110
110		proc_bootstrap();
(gdb) n

Breakpoint 4, boot () at ../../main/main.c:111
111		thread_bootstrap();
(gdb) n

Breakpoint 5, boot () at ../../main/main.c:112
112		hardclock_bootstrap();
(gdb) n
113		vfs_bootstrap();
(gdb) n
114		kheap_nextgeneration();

7. $1 = (struct cpu *) 0x80000

8. $3 = (struct cpu *) 0x8003af00

9. $1 = {arr = {v = 0x0, num = 0, max = 0}}

10.$1 = {arr = {v = 0x80039fe0, num = 1, max = 4}}

