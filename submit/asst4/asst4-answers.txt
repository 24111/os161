1. x7F E L F

2.
enum uio_seg {
        UIO_USERISPACE,			/* User process code. */
        UIO_USERSPACE,			/* User process data. */
        UIO_SYSSPACE,			/* Kernel. */
};

USERISPACE refers to memory buffer in process code in user space
USERSPACE refers to memory buffer for data in user space
SYSSPACE refers to memory buffer in kernel space

You should use SYSSPACE when read/writing data in kernel memory region

3. Because uio is used to read/write memory, and contains the informations about buffer size
and pointer to memory location. The reading action itself uses the information stored in uio.
Memory is read from (or wrote to) the uio buffer to another location (specified by the relevant
functions)

4. Going to user mode means we finished loading the user code. Call vfs_close to mark the vnode for deallocation (by decreasing the ref counter)

5. asm_usermode, called from mips_usermode, from enter_new_process. It does this by setting up a new custom trap frame and call a "fake" interrupt return. The code eventually uses machine dependent locore/exception-mips.s, and is thus machine dependent

6. 
Copyin/out:
	kern/include/copyinout.h (declare)
	kern/vm/copyinout.c (impl)
memmove:
	kern/include/lin.h (declare)
	common/libc/string/memmove.c (impl)
Copyin/out have to ensure the memory region to move from/to stays within its supposed space

7. userptr_t represent a pointer in user space, separated from kernel space pointers.

8. #define EX_SYS    8    /* Syscall */
in: trapframe.h

9.	tf->tf_epc += 4;
Thus, an instruction is 4 bytes long (word)

10. Because it currently panics. Kernel should not panic when it needs to kill threads.
It needs to kill them in cold blood.
So I would probably want to force it to do that.

11. Copy in and start reading from the user space stack pointer.
Where the rest of the params are.

12. Syscall is a huge category
We would probably want to write that somewhere else
Where we can debug it.
In case you're asking for what it actually does, well... it handles system call traps?

13.	Inside kernel: When the trap is found out to be a syscall (EX_SYS)
From userspace: syscall (inside userland/lib/libc/arch/mips/syscalls-mips.S)

14. a0, {a2, a3}, and from userspace stackpointer + 16.
Flag it as 64 bits and join them back. Use split64to32 to make your life easier.
Seriously though. Why can't v1 be the most significant bits.
That way I wouldn't need to flag for 64 bits return.... 

15. vfs_open and all vfs layer functions are used to open files
What vfs does is that it calls down to vnode VOP macros to do its works
Macros that question ask for:
VOP_EACHOPEN
VOP_READ
VOP_WRITE
Files are represented using vnode

16. To keep track of file in use so that:
A. We do not close file in use
B. We do not keep file open when they are no longer used