#include <types.h>
#include <pageswap.h>
#include <lib.h>
#include <bitmap.h>
#include <kern/errno.h>
#include <vm.h>
#include <kern/fcntl.h>
#include <vfs.h>
#include <uio.h>
#include <vnode.h>
#include <kern/stat.h>
#include <coremap.h>
#include <spinlock.h>

struct pageswap *pagesys;
const char *diskname = "lhd0raw:";
//Lock on allocating and deallocating
//We don't need to lock checks, as even if we ended up with a race condition
//checks don't write back and the bit they're checking is not modified
struct spinlock disklock = SPINLOCK_INITIALIZER;

void pageswap_init (void){
	struct pageswap *ps;
	struct vnode *vn;
	char *buf; //We know the name
	struct stat st;
	buf = kstrdup(diskname);
	size_t size;
	ps = kmalloc (sizeof (struct pageswap));
	//We can try to work with a fault mode (no page swapping due to no swap structure)
	//But we have to have the structure at least!
	if (ps == NULL)
		goto panic;

	if (vfs_open(buf, O_RDWR, 0, &vn))
		panic ("fatal error: failed to open paging system disk\n");

	ps->disk = vn;

	VOP_STAT(vn, &st);

	size = coremap_getmemsize();

	if (st.st_size > size * 20)
		size *= 20;
	else
		size = st.st_size;

	kprintf("Paging disk size: %llukb. Used: %ukb.\n", st.st_size, size);
	
	ps->bm = bitmap_create(size);

	if (ps->bm == NULL){
		kprintf("Warning: Low memory on startup. Pageswapping is not enabled!\n");
		size = 0;
	}

	ps->size = size;

	pagesys = ps;

	return;

panic:
	panic ("OOM on kernel startup! - pageswap_init\n");
}

/*
 *	CONCURRENCY ARGUMENT:
 *	Load and store are disc IO operations
 *	It's accessed by coremap to handle its physical memory
 *	Doing so requires the coremap to rewrite data structures
 *	And the coremap should be locked and synchronized, not the swap/store ops
 */
void pageswap_store (vaddr_t addr, uint32_t index){
	//Technically we don't want to store kernel page (!)
	//But swapping tool do not get access to virtual memory, and it does not make sense for it to have access anyway
	//So the logic should have been done in a higher level
	KASSERT((addr & PAGE_FRAME) == addr);			//Making sure it's aligned
	struct uio ku;
	struct iovec iov;


	//Do you have a reservation?
	KASSERT (bitmap_isset(pagesys->bm, index));

	uio_kinit(&iov, &ku, (void *) addr, PAGE_SIZE, (index) * PAGE_SIZE, UIO_WRITE);
	vfs_biglock_acquire();
			VOP_WRITE(pagesys->disk, &ku);
	vfs_biglock_release();
}

void pageswap_load  (vaddr_t addr ,uint32_t index){
	KASSERT((addr & PAGE_FRAME) == addr);			//Making sure it's aligned
	//We doing something meaningful?
	KASSERT (pagesys->size > 0);

	KASSERT (bitmap_isset(pagesys->bm, index));

	struct uio ku;
	struct iovec iov;

	uio_kinit(&iov, &ku, (void *) addr, PAGE_SIZE, (index) * PAGE_SIZE, UIO_READ);	
	vfs_biglock_acquire();
		VOP_READ(pagesys->disk, &ku);	
	vfs_biglock_release();
}

//Wrapper for bitmap alloc essentially
int pageswap_malloc (uint32_t *index){
	int result;
	spinlock_acquire(&disklock);
		result = bitmap_alloc(pagesys->bm, index);
	spinlock_release(&disklock);
	return result;
}

void pageswap_free  (uint32_t index){
	KASSERT (pagesys->size > 0);
	KASSERT (bitmap_isset(pagesys->bm, index));
	spinlock_acquire(&disklock);
		bitmap_unmark(pagesys->bm, index);
	spinlock_release(&disklock);
}