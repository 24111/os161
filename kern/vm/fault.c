#include <types.h>
#include <vm.h>
#include <kern/errno.h>
#include <lib.h>
#include <spl.h>
#include <spinlock.h>
#include <proc.h>
#include <current.h>
#include <mips/tlb.h>
#include <addrspace.h>
#include <coremap.h>
#include <pagetable.h>

int
vm_fault(int faulttype, vaddr_t faultaddress)
{
	vaddr_t vbase1, vtop1, vbase2, vtop2, stackbot, heaptop;
	struct addrspace *as;
	int result;

	faultaddress &= PAGE_FRAME;

	DEBUG(DB_VM, "dumbvm: fault: 0x%x\n", faultaddress);

	switch (faulttype) {
	    case VM_FAULT_READONLY:
	    case VM_FAULT_READ:
	    case VM_FAULT_WRITE:
		break;
	    default:
		return EINVAL;
	}

	if (curproc == NULL) {
		/*
		 * No process. This is probably a kernel fault early
		 * in boot. Return EFAULT so as to panic instead of
		 * getting into an infinite faulting loop.
		 */
		return EFAULT;
	}

	as = proc_getas();
	if (as == NULL) {
		/*
		 * No address space set up. This is probably also a
		 * kernel fault early in boot.
		 */
		return EFAULT;
	}

	/* We can allocate the memory freely inside user space, but there are limitations */
	/* First, check if fault address is out of bound */
	vbase1 = as->as_vbase1;
	vtop1 = vbase1 + as->as_npages1 * PAGE_SIZE;
	vbase2 = as->as_vbase2;
	vtop2 = vbase2 + as->as_npages2 * PAGE_SIZE;
	stackbot = as->stackbot;
	heaptop = as->heapptr;

	if (faultaddress >= vbase1 && faultaddress < vtop1) {
		//User code region
	}
	else if (faultaddress >= vbase2 && faultaddress < vtop2) {
		//User code region
	}
	else if (faultaddress >= HEAP_START && faultaddress < heaptop) {
		//User heap region
	}
	else if (faultaddress >= stackbot && faultaddress < USERSTACK){
		//User stack region
	}
	else if (faultaddress >= stackbot - PAGE_SIZE && faultaddress < stackbot){
		//Stack filled, allocate more space, unless it overflows
		if (stackbot == heaptop)
			return EFAULT;
		as->stackbot-=PAGE_SIZE;
	}
	else{
		//Well, it's not in any of the region it's suppose to be. Just fault it.
		return EFAULT;
	}

	do{
		result = coremap_allocvpage(faultaddress, as->pt, faulttype);
		//If we can't allocate it, no memory is available
		//Most likely it's a filled drive. Each CPU can hold up to 64 pages
		//And kernel can hold up to half the memory
		//So if it fails, likely that we don't even have enough disk to swap
		//Yield and wait for something to get cleaned up
		if (result)
			thread_yield();
	} while (result);

	return 0;
}
