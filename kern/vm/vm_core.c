/* Contains core VM functions dumbvm had */

#include <types.h>
#include <kern/errno.h>
#include <lib.h>
#include <spl.h>
#include <spinlock.h>
#include <proc.h>
#include <current.h>
#include <mips/tlb.h>
#include <addrspace.h>
#include <vm.h>
#include <coremap.h>
#include <pageswap.h>

void
vm_bootstrap(void)
{
	coremap_bootstrap();
}

/* Allocate/free some kernel-space virtual pages */
vaddr_t
alloc_kpages(unsigned npages)
{
	KASSERT(npages > 0);
	vaddr_t va;
	va = coremap_getppages(npages);
	if (va==0) {
		return 0;
	}
	return va;
}

void
free_kpages(vaddr_t addr)
{
	coremap_freeppages(addr);
}

void
vm_tlbshootdown_all(void)
{
	panic("Wait, didn't I make sure this is not USA?!\n");
}

void
vm_tlbshootdown(const struct tlbshootdown *ts)
{
	(void)ts;
	panic("Wait, didn't I make sure this is not USA?!\n");
}

