#include <types.h>
#include <vm.h>
#include <coremap.h>
#include <pageswap.h>
#include <pagetable.h>
#include <spinlock.h>
#include <mips/tlb.h>
#include <kern/errno.h>
#include <thread.h>
#include <lib.h>

//Size of two pointers
#define ENTRY_PER_PAGE (PAGE_SIZE / 8)

struct coremap *cmap;
size_t memsize;

static struct spinlock stealmem_lock = SPINLOCK_INITIALIZER;
//This is to make sure the operating system stop allocating memory a bit earlier
volatile size_t kpage_limit;
volatile uint32_t randomcounter = 15;

//Static function prototypes
static int coremap_seek(uint32_t *index, uint32_t pt_index, struct pagetable *pt);
static void get_random_entry (uint32_t *ehi);
static void coremap_unload (uint32_t ehi);
static int coremap_writetlb (uint32_t chi, uint32_t clo);
static void coremap_dirty_interrupted(uint32_t ehi, struct pagetable *pt);
static void coremap_dirty(uint32_t ehi, struct pagetable *pt);
static int getfreeppages(size_t size, uint32_t *index, int flags);
static void coremap_freevpage(uint32_t index);
static void coremap_kernelclaim(uint32_t index);



/* ================================= BOOTSTRAP CODES ================================= */


void coremap_bootstrap(void){
	struct coremap *map;
	paddr_t last_paddr;

	/* Finding the limits of virtual memory */
	last_paddr = ram_getsize();

	/* Calculate the raw paging */
	memsize = (last_paddr) / PAGE_SIZE;
	last_paddr &= PAGE_FRAME;
	/* Make sure we have enough memory */
	KASSERT (memsize > 1);
	/* First thing in memory! We can't steal mem no more, so do this */
	map = kmalloc(sizeof (*map));
	if (map == NULL)
		panic("Cannot allocate memory for coremap\n");

	//Malloc the maps. Bigger than needed.
	map->tags = kmalloc(sizeof(uint32_t) * memsize);
	if (map->tags == NULL)
		panic("Cannot allocate memory for coremap\n");
	bzero((void*) map->tags, memsize * sizeof(uint32_t));
	map->ptentries = kmalloc(sizeof(void *) * memsize);
	if (map->ptentries == NULL)
		panic("Cannot allocate memory for coremap\n");
	bzero((void*) map->ptentries, memsize * sizeof(void *));

	map->pbase = ram_getfirstfree();
	//Must align
	KASSERT((map->pbase & PAGE_FRAME) == map->pbase);

	map->memsize = (last_paddr - map->pbase) / PAGE_SIZE;
	spinlock_init(&map->lock);

	cmap = map;
	//Kernel shouldn't eat up more than half the memory (This can be adjusted)
	//Stops creating too many processes. Ensure that the running processes is allowed to run
	//This limit is calculated base on memsize intentionally, so kernel have slightly more
	//Than half the memory. This helps kmalloc tests, etc, for 512kb of RAM
	//A more complicated function could be used in the future
	kpage_limit = memsize / 2;
}

size_t coremap_getmemsize(void){
	return memsize;
}

/* ================================= MEMORY ALLOCATION/ READING COREMAP ================================= */
/* ========================== INTENDED AS HELPER TOOLS FOR MORE COMPLICATED OPS ========================= */
/* ============================== STRICTLY REQUIRES HOLDING OF CMAP LOCK ================================ */
/*
 *	Scan coremap for matching entry
 */
static int coremap_seek(uint32_t *index, uint32_t pt_index, struct pagetable *pt){
	uint32_t i;
	uint32_t coremap_data;

	for (i = 0; i < cmap->memsize; i++){
		if (cmap->ptentries[i] == pt){
			coremap_data = CM_DAT(cmap->tags[i]);
			if (coremap_data == pt_index){
				*index = i;
				return 0;
			}
		}
	}
	return 1;
}

//Filled tlb. We need to do something!
//So get a random ehi entry and report back
//It's best to limit tlb access, so don't modify it
static void get_random_entry (uint32_t *ehi){
	//We might be holding lock, but we don't need to

	uint32_t elo;
	//Randomize a starting index
	unsigned k = randomcounter % NUM_TLB;
	randomcounter++;

	tlb_read (ehi, &elo, (int) k);

	//If we fetched an invalid entry, why the hell did we even call get random entry?
	KASSERT (elo & TLBLO_VALID);
}

//Unmark the entry from being used. Unused entries can be swapped without firing a tlb shootdown
//Does not clear dirty bit or valid bit. Dirty is cleared by swapping, and valid is cleared by unloading a process
static void coremap_unload (uint32_t ehi){
	KASSERT (spinlock_do_i_hold(&cmap->lock));
	int i;
	uint32_t index, elo;

	//Page aligned?
	KASSERT ((ehi & PAGE_FRAME) == ehi);
	
	//Read the tlb for entrylo
	i = tlb_probe(ehi, 0);

	//We're trying to unload something that's not in use, fail
	KASSERT (i >= 0);

	tlb_read(&ehi, &elo, i);
	elo &= PAGE_FRAME;		//Clear the sigs

	index = (elo - cmap->pbase) >> CM_SHIFT;

	//Are we updating the INUSE bit properly?
	KASSERT (cmap->tags[index] & INUSE);

	//Clear the INUSE bit
	cmap->tags[index] &= ~INUSE;

	//Clearing tlb entry
	tlb_write(TLBHI_INVALID(i), TLBLO_INVALID(), i);
}

//Writes to the tlb free location.
//If tlb is filled, report back
//Mark the page as in use (duh)
static int coremap_writetlb (uint32_t chi, uint32_t clo){
	KASSERT (spinlock_do_i_hold(&cmap->lock));
	int i, dirty = 0;
	uint32_t ehi, elo, index;

	//Must be aligned
	KASSERT ((clo & PAGE_FRAME) == clo);
	KASSERT ((chi & PAGE_FRAME) == chi);

	index = ((clo & PAGE_FRAME) - cmap->pbase) >> CM_SHIFT;

	//Process memory sharing is disabled. Kernel is always INUSE, so check for only INUSE bit
	KASSERT ((cmap->tags[index] & INUSE) == 0);
	if (cmap->tags[index] & DIRTY)
		dirty = TLBLO_DIRTY;
	for (i=0; i<NUM_TLB; i++) {
		tlb_read(&ehi, &elo, i);
		if (elo & TLBLO_VALID) {
			continue;
		}

		cmap->tags[index] |= INUSE | VALID; 
		tlb_write(chi , clo | TLBLO_VALID | dirty, i);
		return 0;
	}

	return 1;
}

/*
 * Mark a page as dirty and enable r-w
 * This is the only way a page can get dirty
 * Uninitiated pages will technically trigger a vm_fault anyway
 * Note that we need to check if the entry is loaded or not
 * Since another interrupt might have kicked in and flushed our entry
 * Unlike the easier cases, we're not sure if it's in our tlb or not (definitely not in the other case)
 */
static void coremap_dirty(uint32_t ehi, struct pagetable *pt){
	uint32_t elo, index;
	int i;

	KASSERT(spinlock_do_i_hold(&cmap->lock));
	
	//Read the tlb
	i = tlb_probe(ehi, 0);

	if (i < 0){
		//Ok, not cool. We don't even know if our page is still on disk
		//Call a specialized handler
		//This handler will cause the tlb to look as if it was not interrupted
		coremap_dirty_interrupted(ehi, pt);
		i = tlb_probe(ehi, 0);
		KASSERT (i >= 0);
	}

	tlb_read(&ehi, &elo, i);

	//This prevents us from calling coremap_dirty from where we're not supposed to
	KASSERT ((elo & DIRTY) == 0);

	
	//Align the paddr
	elo &= PAGE_FRAME;
	index = (elo - cmap->pbase) >> CM_SHIFT;
	//Same as above, making sure we're not marking a clean page dirty
	KASSERT ((cmap->tags[index] & DIRTY) == 0);
	cmap->tags[index] |= DIRTY;

	tlb_write(ehi, elo | TLBLO_DIRTY | TLBLO_VALID, i);
}

/*
 *	We got timer interrupted and flushed our tlb.
 *	Now, we aren't so sure if the page is still on disk.
 *	The inuse bit is definitely dead
 */
static void coremap_dirty_interrupted(uint32_t ehi, struct pagetable *pt){
	uint32_t i, data, index, pt_index, elo;
	uint32_t rep_data;
	uint32_t rep_disk;
	int result;
	int dirty = 0;

	for (i = 0; i < pt->size; i++){
		if (pt->vaddrs[i] == ehi){
			data = pt->indexes[i];
			pt_index = i;
			break;
		}
	}

	//We can't service a dirty page that doesn't exist. New pages are always clean.
	KASSERT(i < pt->size);

	//Now we have the data. Use it to determine our action
	if (PAGETABLE_INMEM(data)){
		//Great! it's still in memory, write the entry back (we are uninterrupted while holding the coremap lock)
		result = coremap_seek(&index, pt_index, pt);
		elo = cmap->pbase + index * PAGE_SIZE;
		//This writes the tlb entry back as not dirty
		result = coremap_writetlb(ehi, elo);
		if (result){
			panic ("This roundtine is called when we got flushed in kernel, we should have an empty TLB!\n");
		}
	} else {
		//great, we got interrupted and said page is evicted?
		//*sigh* tries to grab another page. This could take a while.
		do{
			result = getfreeppages(1, &index, VALID);

			if (result)
				result = getfreeppages(1, &index, INUSE);
				
			if (result){
				//No memory available. Wait a bit
				spinlock_release(&cmap->lock);
				thread_yield();
				spinlock_acquire(&cmap->lock);
			}
		} while(result);

		//Find the actual kernel address
		elo = cmap->pbase + index * PAGE_SIZE;
		//Great, we have a page. Now load it up
		//Skip the page cleanup if we don't have to
		//This was copied from getvpage, maybe I should make another function for this
		if ((cmap->tags[index] & VALID) == 0)
			goto not_used_dirty;

		//Flag the entry first. Then see if we need towing
		rep_data = CM_DAT(cmap->tags[index]);
		KASSERT (cmap->ptentries[index]->indexes[rep_data] & PT_INMEM);
		cmap->ptentries[index]->indexes[rep_data] &= ~PT_INMEM;

		dirty = cmap->tags[index] & DIRTY;

		//Now, we inform the poor fella if his page's been towed (parking ticket joke)
		if (dirty){
			rep_disk = PAGETABLE_INDEX(cmap->ptentries[index]->indexes[rep_data]);
			cmap->ptentries[index]->indexes[rep_data] |= PT_TOWED;

			cmap->tags[index] |= RESERVED;
			spinlock_release (&cmap->lock);
				pageswap_store(PADDR_TO_KVADDR(elo) , rep_disk);
			spinlock_acquire (&cmap->lock);
			cmap->tags[index] &= ~RESERVED;	//We don't need to, but it's good practice

			cmap->ptentries[index]->indexes[rep_data] &= (~PT_TOWED);
			cmap->ptentries[index]->indexes[rep_data] |= PT_VALID;
		
		}

not_used_dirty:
		//Now, we claim the spot as our own
		cmap->ptentries[index] = pt;
		cmap->tags[index] = (pt_index) << CM_SHIFT;
		//We will be doing IO
		cmap->tags[index] |= RESERVED;
		bzero((void *)PADDR_TO_KVADDR(elo), PAGE_SIZE);


		//Our page is gone. It might still be towed
		while (pt->indexes[pt_index] & PT_TOWED){
			spinlock_release (&cmap->lock);
			thread_yield();
			spinlock_acquire (&cmap->lock);
		}
		spinlock_release (&cmap->lock);
			//Clears memory for loading. We don't even need to check valid: We got evicted!
			pageswap_load(PADDR_TO_KVADDR(elo), PAGETABLE_INDEX(data));

		spinlock_acquire (&cmap->lock);
		cmap->tags[index] &= ~RESERVED;
		//Load complete, return
		result = coremap_writetlb(ehi, elo);
		if (result)
			panic ("This rountine is called when we got flushed in kernel, we should have an empty TLB!\n");
		pt->indexes[pt_index] |= PT_INMEM;
	}
}

void coremap_flushtlb(){
	int i;
	uint32_t ehi, elo;

	spinlock_acquire (&cmap->lock);
		for (i=0; i<NUM_TLB; i++) {
			tlb_read(&ehi, &elo, i);
			if (elo & TLBLO_VALID) {
				coremap_unload(ehi);
			}
		}
	spinlock_release (&cmap->lock);
}

/* ============= SEEK AND ALLOCATION ================= */
//User always set size = 1, flags are conditions to avoid
//Normally flag = valid, if that fails, flag = inuse
static int getfreeppages(size_t size, uint32_t *index, int flags){
	KASSERT (spinlock_do_i_hold(&cmap->lock));
	unsigned i;
	//A bit of randomness so we don't try to use the same page again and again
	int k = randomcounter % (cmap -> memsize);
	randomcounter++;

	//But there's no point if we're just looking for unused page
	if (flags & VALID)
		k = 0;

	//Noone have the right to touch reserved page.
	flags |= RESERVED;


	size_t count = 0;

	//Search from k first
	for (i = k; i < cmap->memsize; i++){
		if (cmap->tags[i] & flags){
			//Reached a valid page/ in use page, continue
			count = 0;
		}
		else{
			count++;
			if (count == size){
				//Found
				*index = i + 1 - size;
				return 0;
			}
		}
	}

	//Not a random search, we're finished
	if (flags & VALID)
		return 1;

	//Suppose to loop to k+size , or memsize. I'm lazy and this is more bug free
	for (i = 0; i < cmap->memsize; i++){
		if (cmap->tags[i] & flags){
			//Reached a valid page/ in use page, continue
			count = 0;
		}
		else{
			count++;
			if (count == size){
				//Found
				*index = i + 1 - size;
				return 0;
			}
		}
	}
	//Not found
	return 1;
}

/*
 * Remove reference, like swapping, but we don't need to do much signaling
 * This is wrapper for coremap_freevpages function. It takes care of any cleaning
 * The coremap entry might need to do
 * For now, it's just making sure we're not cleaning up pages INUSE
 * Because: pages are free on as killing
 * AS killing means it's not active
 * If it's not active, it can't be INUSE
 */
static void coremap_freevpage(uint32_t index){
	KASSERT (spinlock_do_i_hold(&cmap->lock));
	KASSERT ((cmap->tags[index] & INUSE) == 0);
	cmap->tags[index] = 0;
	cmap->ptentries[index] = NULL;
}


/* ================================= KERNEL MEMORY ALLOCATION (KMALLOC) ================================= */
//This is called by coremap_getppages only. 
static void coremap_kernelclaim(uint32_t index){
	struct pagetable *pt;
	uint32_t data;

	KASSERT ((cmap->tags[index] & (RESERVED | DIRTY | INUSE)) == 0);

	pt = cmap->ptentries[index];
	cmap->ptentries[index] = NULL;
	data = cmap->tags[index];
	cmap->tags[index] = KERNEL | VALID | INUSE;

	if (pt){
		KASSERT (data & VALID);
		data = CM_DAT(data);
		pt->indexes[data] &= ~PT_INMEM;
	}
}

//Allocate some free physical pages, swapping if neccessary
//Page allocated by 
//Used by kmalloc. Will return a kseg0 address
vaddr_t coremap_getppages(size_t size){
	int result;
	uint32_t index, i;
	vaddr_t stolen_meme;	//Yes, this is intentional

	//Not initialized
	if (cmap == NULL){	
		spinlock_acquire(&stealmem_lock);
			stolen_meme = ram_stealmem(size);
		spinlock_release(&stealmem_lock);
		//Turn to kernel, so we can zero it
		stolen_meme = PADDR_TO_KVADDR(stolen_meme);
		bzero((void *) stolen_meme, size * PAGE_SIZE);
		return stolen_meme;
	}

	spinlock_acquire(&cmap->lock);
		if (kpage_limit < size){
			//Uh oh, kernel is using too much memory
			spinlock_release(&cmap->lock);
			return 0;
		}

		//We can't fetch dirty page. Kmalloc can't evict
		result = getfreeppages(size, &index, VALID | DIRTY);

		if (result)
			result = getfreeppages(size, &index, INUSE | DIRTY);
			
		if (result){
			//No memory available
			spinlock_release(&cmap->lock);
			return 0;
		}

		kpage_limit -= size;

		//Update the coremap
		//All pagetable pointers should be NULL
		//Writes the remaining entries
		//The first entry should contain the size
		for (i = 0; i < size; i++){
			//Since we might be kicking people off the mem
			//Do have some decency and let them know
			coremap_kernelclaim(index + i);	
		}
		cmap->tags[index] = KENTRY(size);
	spinlock_release(&cmap->lock);

	stolen_meme = PADDR_TO_KVADDR(cmap->pbase + (index << CM_SHIFT));
	bzero((void *) stolen_meme, size * PAGE_SIZE);
	return stolen_meme;
}

void coremap_freeppages(vaddr_t vaddr){
	uint32_t index, len, i;
	paddr_t paddr;

	//Aligned? In range?
	KASSERT ((vaddr & PAGE_FRAME) == vaddr);
	//We need the actual address, so change that
	paddr = vaddr - MIPS_KSEG0;
	if (paddr < (cmap->pbase)){
		//Untracked memory region. Just return.
		return;
	}
	//Now grab the index in the coremap
	index = (paddr - cmap->pbase) >> CM_SHIFT;
	//At this point we need to modify coremap, so lock it
	spinlock_acquire(&cmap->lock);
		//Must be freeing kernel memory
		KASSERT (cmap->tags[index] & KERNEL);
		len = CM_DAT(cmap->tags[index]);
		//Must be a head address, since trailing pages do not have length data for safety reasons
		KASSERT (len > 0);
		for (i = 0; i < len; i++){
			//Mark as not kernel, not in use
			//No need to touch ptentries, already null
			cmap->tags[index + i] = 0;
		}
		kpage_limit += len;
	spinlock_release(&cmap->lock);
}

/* ================================= USER PAGING ================================= */
/*
 * Called by VM system to allocate a new user page
 * Also handles VM allocation and VM faults
 * Flag informs us if a page is dirty
 * Now, we can actually got interrupted while resolving this interrupt! (I found out the hard way)
 * So, our tlb might not be a good indicator
 * Just some rant since I tried to read tlb for servicing interrupt
 */
int coremap_allocvpage(vaddr_t vpage, struct pagetable *pt, int flag){
	uint32_t i, index, pt_index, pt_data;
	uint32_t ehi, elo, evict_ehi;
	uint32_t rep_data;	//Data of the replaced page, if any
	uint32_t rep_disk;
	int result;
	int found = 0;
	int dirty = 0;
	//Align the vpage
	vpage &= PAGE_FRAME;
	//Hmn... how?
	KASSERT (vpage < MIPS_KSEG0); 
	//Can't go around accessing NULL pointers
	KASSERT (pt);

	spinlock_acquire (&cmap->lock);
		//Is this the really easy case of write-on-readonly?
		if (flag == VM_FAULT_READONLY){
			coremap_dirty(vpage, pt);
			spinlock_release(&cmap->lock);
			return 0;
		}

		//Ok, it's not.
		//Which means it could be a completely new insertion

		//First, we dig the pagetable for a valid entry. If we do, set a flag
		for (i = 0; i < pt->size; i++){
			if (pt->vaddrs[i] == vpage){
				found = 1;
				pt_index = i;
				break;
			}
		}

		//It is a new entry. Allocate it
		if (!found){
			result = pagetable_insert(pt, vpage);
			//Failed to allocate a new disk because of whatever reason
			//Return the error and let whoever that needed a new page take care of it (sadly, it's me)
			if (result){
				spinlock_release(&cmap->lock);
				return result;
			}
			//Take advantage that we know that the pagetable fills up linearly
			pt_index = pt->size - 1;
		}

		//Great, now our page have a backup (whenever we just assigned or not), but we still need to find some space for it
		//Let's check if the page is in memory (this will fail if we just assign our page above, no worries)
		pt_data = pt->indexes[pt_index];
		if (PAGETABLE_INMEM(pt_data)){
			result = coremap_seek(&index, pt_index, pt);
			//Making sure we did find the entry
			KASSERT (result == 0);

			//Great, load the entry to the tlb and finish 
			ehi = vpage;
			elo = cmap->pbase +(index << CM_SHIFT);
			result = coremap_writetlb(ehi, elo);
			if (result)	{
				//Uh oh, is the tlb filled?
				get_random_entry (&evict_ehi);
				coremap_unload(evict_ehi);

				result = coremap_writetlb(ehi, elo);
				if (result)
					panic("But... I just freed an entry!");
			}
			//and... DONE!
			spinlock_release(&cmap->lock);
			return 0;
		}
		
		//Alright, it wasn't any of the easy cases.
		//First, grab ourself a free entry
		do{
			result = getfreeppages(1, &index, VALID);

			if (result)
				result = getfreeppages(1, &index, INUSE);
				
			if (result){
				//No memory available. Wait a bit
				spinlock_release(&cmap->lock);
				thread_yield();
				spinlock_acquire(&cmap->lock);
			}
		} while(result);

		KASSERT ((cmap->tags[index] & INUSE) == 0);

		//We know ehi and elo now, so load it up
		ehi = vpage;
		elo = cmap->pbase + index * PAGE_SIZE;

		//Now, we have an index to use. First, decide if we need to clean up the old entry
		if ((cmap->tags[index] & VALID) == 0)
			goto not_used;
		

		rep_data = CM_DAT(cmap->tags[index]);
		//We do. Flag the entry as in mem first
		KASSERT ( cmap->ptentries[index]->indexes[rep_data] & PT_INMEM );
		cmap->ptentries[index]->indexes[rep_data] &= ~PT_INMEM;

		//Now check if it's dirty
		dirty = cmap->tags[index] & DIRTY;
		
		//IO for dirty
		if (dirty){
			//Find the disk location
			rep_disk = PAGETABLE_INDEX(cmap->ptentries[index]->indexes[rep_data]);
			//We inform the poor fella if his page's been towed (parking ticket joke)
			cmap->ptentries[index]->indexes[rep_data] |= PT_TOWED;
			cmap->tags[index] |= RESERVED;

			//Unlock to do IO
			spinlock_release (&cmap->lock);
				pageswap_store(PADDR_TO_KVADDR(elo), rep_disk);
			spinlock_acquire (&cmap->lock);
			
			cmap->tags[index] &= ~RESERVED;
			KASSERT(cmap->ptentries[index]);
			cmap->ptentries[index]->indexes[rep_data] &= ~PT_TOWED;
			cmap->ptentries[index]->indexes[rep_data] |= PT_VALID;
		}

not_used:
		//Even if it was in use, it is certainly not now
		//Now, we claim the spot as our own
		cmap->ptentries[index] = pt;
		cmap->tags[index] = (pt_index) << CM_SHIFT;

		//Zero the area in preparation
		bzero((void *)PADDR_TO_KVADDR(elo), PAGE_SIZE);

		//Now check if we need to load data
		if (PAGETABLE_VALID (pt_data)){

			//We're still reserving this page for IO
			cmap->tags[index] |= RESERVED;

			//Just making sure our page isn't towed (towed page will show as in disk)
			while (pt->indexes[pt_index] & PT_TOWED){
				spinlock_release (&cmap->lock);
				thread_yield();
				spinlock_acquire (&cmap->lock);
			}
			//Now do the load IO
			spinlock_release (&cmap->lock);

			pageswap_load(PADDR_TO_KVADDR(elo), PAGETABLE_INDEX(pt_data));

			spinlock_acquire (&cmap->lock);
			cmap->tags[index] &= ~RESERVED;

		}

		//Now just write the tlb and return
		result = coremap_writetlb(ehi, elo);
		if (result)	{
			//Uh oh, is the tlb filled?
			get_random_entry (&evict_ehi);
			coremap_unload(evict_ehi);

			result = coremap_writetlb(ehi, elo);
			if (result)
				panic("But... I just freed an entry!");
		}
		pt->indexes[pt_index] |= PT_INMEM;
		
	spinlock_release (&cmap->lock);
	return 0;
}

/*
 *	Revoke all memory assigned to a process (pagetable).
 *	Called on address space destroy
 */
void coremap_freevpages(struct pagetable *pt){
	uint32_t *list;
	uint32_t data, i;
	
	//Can't be NULL
	KASSERT(pt);

	list = pt->indexes;
	spinlock_acquire(&cmap->lock);
		//First, we clean up the disk allocations
		for (i = 0; i < pt->size; i++){
			data = PAGETABLE_INDEX(list[i]);
			pageswap_free(data);
		}

		//Then, we clean up the coremap
		for (i = 0; i < cmap->memsize; i++){
			if (cmap->ptentries[i] == pt)
				coremap_freevpage(i);
		}
	spinlock_release(&cmap->lock);
}

/* ================================= FORK SUPPORT ================================= */
int coremap_forkmem(struct pagetable *new, struct pagetable *old){
	int result;
	uint32_t index, i, j, data;
	void *buffer;
	vaddr_t kaddr;

	//Buffer acts as a middle bridge to load memory to disk
	buffer = kmalloc(PAGE_SIZE);
	if (buffer == NULL)
		return ENOMEM;

	spinlock_acquire(&cmap->lock);
		//Allocate the disk blocks. If it fails, we don't have enough memory
		for (i = 0; i < old->size; i++){
			result = pageswap_malloc(&index);
			if (result){
				//Free the allocated swap pages. Note that we need to free to i-1, but i is unsigned so we can't check -1
				for (j = 0; j < i; j++){
					pageswap_free(PAGETABLE_INDEX(new->indexes[j]));
				}
				kfree(buffer);
				spinlock_release(&cmap->lock);
				return ENOMEM;
			}
			//All new entries are valid
			new->indexes[i] = PAGETABLE_INIT(index) | PT_VALID;
		}

		//Now, we copy the data to disk, through the buffer if needed
		for (i = 0; i < old->size; i++){
			data = old->indexes[i];
			if (PAGETABLE_INMEM(data)){
				result = coremap_seek(&index, i, old);
				KASSERT (result == 0);
				//Now we do some IO
				cmap->tags[index] |= RESERVED;
				kaddr = PADDR_TO_KVADDR(cmap->pbase + (index << CM_SHIFT));
				spinlock_release(&cmap->lock);

				pageswap_store(kaddr, PAGETABLE_INDEX(new->indexes[i]));

				spinlock_acquire(&cmap->lock);
				cmap->tags[index] &= (~RESERVED);

			} else {
				//Surprisingly, this is easier

				//First, make sure it's not towed
				while (old->indexes[i] & PT_TOWED){
					spinlock_release (&cmap->lock);
					thread_yield();
					spinlock_acquire (&cmap->lock);
				}

				//Then we do IO, except no seeking or reserving needed
				spinlock_release(&cmap->lock);


				bzero(buffer, PAGE_SIZE);
				
				pageswap_load ((vaddr_t) buffer, PAGETABLE_INDEX(old->indexes[i]));
				pageswap_store((vaddr_t) buffer, PAGETABLE_INDEX(new->indexes[i]));

				spinlock_acquire(&cmap->lock);
			}
		}

	spinlock_release(&cmap->lock);
	kfree(buffer);
	return 0;
}