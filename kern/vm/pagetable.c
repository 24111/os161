#include <types.h>
#include <pagetable.h>
#include <kern/errno.h>
#include <lib.h>
#include <coremap.h>
#include <pageswap.h>

/* Return if the pt is filled and needs to be extended */
static int filled (struct pagetable *pt){
	int i;
	//Min size
	if (pt->size < PAGETABLE_MIN)
		return 0;
	for (i = 2; i < 31; i++){
		if ((pt->size & (1 << i)) == pt->size)
			return 1;
	}
	return 0;
}

struct pagetable *pagetable_create (void){
	struct pagetable *pt;

	pt = kmalloc(sizeof (struct pagetable));
	if (pt == NULL)
		return NULL;
	pt->size = 0;
	
	pt->vaddrs = kmalloc(sizeof (vaddr_t) * PAGETABLE_MIN);
	if (pt->vaddrs == NULL){
		kfree(pt);
		return NULL;
	}

	pt->indexes = kmalloc(sizeof (uint32_t) * PAGETABLE_MIN);
	if (pt->indexes == NULL){
		kfree(pt->vaddrs);
		kfree(pt);
		return NULL;
	}
	return pt;
}

void pagetable_destroy (struct pagetable *pt){
	KASSERT(pt != NULL);
	
	kfree(pt->vaddrs);
	kfree(pt->indexes);
	kfree(pt);
}

//Newly inserted pagetable entry is in memory
int pagetable_insert (struct pagetable *pt, vaddr_t vaddr){
	vaddr_t *ext_vaddrs;
	uint32_t *ext_indexes;
	uint32_t size;
	//Index of the new entry
	uint32_t index, i;
	int result;

//No dupes please
	for (i = 0; i < pt->size; i++)
		if (pt->vaddrs[i] == vaddr)
			return 0;

	result = pageswap_malloc (&index);
	if (result)
		return result;


	/* Did we exceed the current pagetable size? */
	if (filled(pt)){
		/* Malloc the new array */
		size = (pt->size + 1) * 2;
		ext_vaddrs = kmalloc (sizeof(vaddr_t) * size);
		if (ext_vaddrs == NULL){
			pageswap_free(index);
			return ENOMEM;
		}
		ext_indexes = kmalloc (sizeof(uint32_t) * size);
		if  (ext_indexes == NULL){
			pageswap_free(index);
			kfree(ext_vaddrs);
			return ENOMEM;
		}

		/* Copy the content*/
		memcpy(ext_indexes, (const void *) pt->indexes, pt->size * sizeof (uint32_t));
		memcpy(ext_vaddrs, (const void *) pt->vaddrs, pt->size * sizeof (vaddr_t));

		/* Freeing the old arrays */
		kfree(pt->indexes);
		kfree(pt->vaddrs);

		/* Assign the new arrays*/
		pt->indexes = ext_indexes;
		pt->vaddrs = ext_vaddrs;
	}

	pt->vaddrs[pt->size] = vaddr;
	//Not allocated
	pt->indexes[pt->size] = PAGETABLE_INIT(index);

	pt->size++;
	return 0;
}

//Other cpu can modify the indexes. That's safe if we don't read/modify the indexes
struct pagetable *pagetable_copy (struct pagetable *pt){
	struct pagetable *ptclone;
	size_t size;
	int i, result;
	vaddr_t *vaclone;
	uint32_t *inclone;

	size = pt->size;
	//We're forking. Size shouldn't be zero!
	KASSERT (size > 0);	
	//Shift size to 2^n to get actual pagetable size
	for (i = 1; size != 1; i++)
		size >>= 1;
	for (; i > 0; i--)
		size <<= 1;
	//Minimum size case
	if (size < PAGETABLE_MIN)
		size = PAGETABLE_MIN;

	vaclone = kmalloc (size * sizeof (vaddr_t));
	if (vaclone == NULL)
		return NULL;
	
	inclone = kmalloc (size * sizeof (vaddr_t));
	if (inclone == NULL){
		kfree(vaclone);
		return NULL;
	}
	
	ptclone = kmalloc (sizeof (struct pagetable));
			if (ptclone == NULL){
				kfree(inclone);
				kfree(vaclone);	
		return NULL;
	}

	//Copy the information over. Index is not needed, as we will assign new data there anyway
	memcpy(vaclone, (const void *) pt->vaddrs, pt->size * sizeof (vaddr_t));

	ptclone->indexes = inclone;
	ptclone->vaddrs = vaclone;
	ptclone->size = pt->size;
	//Now we call the coremap to handle the actual data IO
	result = coremap_forkmem(ptclone, pt);

	if (result)
		return NULL;
	
	return ptclone;
}