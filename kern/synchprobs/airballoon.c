/*
 * Driver code for airballoon problem
 */
#include <types.h>
#include <lib.h>
#include <thread.h>
#include <test.h>
#include <synch.h>
#include <spinlock.h>	
#include <current.h>

#define NROPES 64		//Must be 2^n, n >= 2. Just because it makes my life easier
#define ALLRAND 0 		//If you don't use 2^n, switch this to 1
#define DRAND_S 12 		//Seeds
#define MRAND_S 14
#define FRAND_S 11

static int ropes_left = NROPES;
static struct lock *rope_lock;			//lock for rope count

// Data structures for rope mappings

struct rope {								//BLIND: You can't know what pole a rope is tied to!
	int num;
	struct quicklock *lock;						//Protect cut value
	volatile bool cut;
	struct rope *rope_b;
	struct rope *rope_a;
};

struct rope *rope_create(int num);
void rope_destroy (struct rope *);

struct pole{
	int num;
	struct quicklock *lock;		//Protect rope chain (rope_a and rope_b)
	struct rope *rope_a;		//First rope in chain
};

struct pole *pole_create(int num, struct rope *);
void pole_destroy(struct pole *);

struct rope *get_rope_from_pole(struct pole *);
bool try_and_get_rope(struct rope *);
bool place_rope(struct rope *, struct pole *, struct pole *);
void cut(struct rope *);

struct quicklock{
    struct spinlock lock;
    struct thread *holder;
};

struct quicklock *quicklock_create(void);
void quicklock_destroy(struct quicklock *);
bool quicklock_try_and_get(struct quicklock *);
void quicklock_release(struct quicklock *);
bool quicklock_do_i_hold(struct quicklock *);



//IMPLEMENTATIONS

struct rope *rope_create(int num){
	struct rope *rope;
	rope = kmalloc(sizeof(struct rope));

	if (rope == NULL)
		return NULL;
	rope->lock = quicklock_create();
	if (rope->lock == NULL){
		kfree(rope);
		return NULL;
	}
	rope->num = num;
	rope->cut = false;
	rope->rope_a = NULL;
	rope->rope_b = NULL;
	return rope;
}

//Destroy the rope, NOT its chaining components;
void rope_destroy(struct rope* rope){
	KASSERT (rope != NULL);
	quicklock_destroy(rope->lock);
	kfree(rope);
}

struct pole *pole_create(int num, struct rope *rope){
	struct pole *pole;
	pole = kmalloc(sizeof(pole));

	if (pole == NULL)
		return NULL;
	pole->lock = quicklock_create();
	if (pole->lock == NULL){
		kfree(pole);
		return NULL;
	}
	pole->num = num;
	pole->rope_a = rope;
	return pole;
}

//Destroy the pole, NOT the rope
void pole_destroy(struct pole *pole){
	KASSERT(pole != NULL);
	quicklock_destroy(pole->lock);
	kfree(pole);
}

/*
 * Get an uncut rope from a pole. Acquire the rope lock in the process. NULL if pole have no rope or is blocked
 */
struct rope *get_rope_from_pole(struct pole *pole){
	if (!quicklock_try_and_get(pole->lock))					//We're touching 
		return NULL;
	struct rope *rope = pole->rope_a;
	while (rope != NULL){
		if (try_and_get_rope(rope)){
			if (rope->rope_a != NULL)						//Update the chain. Not really needed for marigold.
				rope->rope_a->rope_b =rope->rope_b;
			if (rope->rope_b != NULL)
				rope->rope_b->rope_a = rope->rope_a;
			quicklock_release(pole->lock);					//Release pole lock for Killer/Mari
			return rope;
		}
		else
			rope = rope->rope_a;
	}
	quicklock_release(pole->lock);
	return NULL;
}

/*
 * Acquire the rope lock if uncut and not taken. Return false otherwise.
 */
bool try_and_get_rope(struct rope* rope){
	if (quicklock_try_and_get(rope->lock)){
		if(rope->cut){
			quicklock_release(rope->lock);		//Rope cut, release lock and return
			return false;
		}
		return rope;	
	}
	else
		return false;							//Someone else has it, try again
}

/*
 *	Called by flowerkiller to place the ropes. Return true on succesful try.
 */
bool place_rope(struct rope *rope, struct pole *pole, struct pole *origin_pole){
	KASSERT(quicklock_do_i_hold(rope->lock));	//Must hold rope lock

	if(!quicklock_try_and_get(pole->lock))
		return false;							//Marigold is here, try another pole

	if (pole == origin_pole)
		return false;							//Uh oh, I'm not doing anything useful!

	rope->rope_b = NULL;
	rope->rope_a = pole->rope_a;
	pole->rope_a = rope;						//Place the rope
	kprintf("Lord FlowerKiller switched rope %d from stake %d to stake %d\n",
		rope->num, origin_pole->num, pole->num);	//First announce your deed
	quicklock_release(rope->lock);				//Then we release the lock! Release rope lock first so Marigold don't skip it
	quicklock_release(pole->lock);
	return true;								//Success!
}

void cut(struct rope *rope){
	KASSERT(quicklock_do_i_hold(rope->lock));	//Don't cut someone else's rope!
	rope->cut = true;
	lock_acquire(rope_lock);
	ropes_left--;
	lock_release(rope_lock);					//Could busy wait here, since it's very short, but just in case
	quicklock_release(rope->lock);				//It's safe to release the lock, this rope is cut!
} 


//Data protection lock. A lock that does not put a thread to sleep

struct quicklock *quicklock_create(){
	struct quicklock *qlock;
	qlock = kmalloc(sizeof(struct quicklock));
	if (qlock == NULL)
		return NULL;
	spinlock_init(&qlock->lock);
	qlock->holder = NULL;
	return qlock;
}

void quicklock_destroy(struct quicklock *qlock){
	KASSERT(qlock != NULL);
	spinlock_cleanup(&qlock->lock);
	kfree (qlock);
}

bool quicklock_try_and_get(struct quicklock *qlock){
	KASSERT(qlock != NULL);
	spinlock_acquire(&qlock->lock);
	if (qlock->holder != NULL){
		spinlock_release(&qlock->lock);
		return false;
	}
	qlock->holder = curthread;
	spinlock_release(&qlock->lock);
	return true;
}

void quicklock_release(struct quicklock *qlock){
	spinlock_acquire(&qlock->lock);
	if (!quicklock_do_i_hold(qlock)){
		spinlock_release(&qlock->lock);
		return;
	}
	qlock->holder = NULL;
	spinlock_release(&qlock->lock);
}

bool quicklock_do_i_hold(struct quicklock *qlock){
	return qlock->holder == curthread;
}

/*
 * Describe your design and any invariants or locking protocols 
 * that must be maintained. Explain the exit conditions. How
 * do all threads know when they are done?  
 */

static struct semaphore *bal_sem;		//Balloon sleeps here
static struct semaphore *threads_sem;	//Main sleeps here
static struct rope *ropes[NROPES];
static struct pole *poles[NROPES];

/*
 * Linear congruential generator
 * Not the best practice to re-generate parameters from only 3 seeds
 * But I'm lazy
 * Number will cycle from 1 to 16, so efficient!
 */
static int getNext(int curr, int seed){
	if (ALLRAND)
		goto allr;
	int multiplier = (seed & 3)*4 + 1;				//0 < Multiplier = 4k+1 < NROPES
	int increment = (seed & (NROPES/2 - 1)) * 2 + 1;		//0 < increment = 2k+1 < NROPES
	return (curr*multiplier + increment)&(NROPES - 1);
allr:
	return (curr + 1) % NROPES;					//Less random but work everywhere

}

static
void
dandelion(void *p, unsigned long arg)
{
	(void)p;
	(void)arg;
	int index = DRAND_S % NROPES;
	struct rope *rope;

	kprintf("Dandelion thread starting\n");

	while (ropes_left > 0){
		index = getNext(index, DRAND_S);
		if (try_and_get_rope(ropes[index])){							//Try to acquire rope lock
			rope = ropes[index];
			kprintf("Dandelion severed rope %d\n", rope->num);			//Noone can touch the rope anyway! Safe to declare
			cut(rope);													//Release rope lock upon completion
		}
	}

	V(bal_sem);				//Release the balloon! Can be done in either dandelion or marigold thread
	kprintf("Dandelion thread done\n");
	V(threads_sem);
}

static
void
marigold(void *p, unsigned long arg)
{
	(void)p;
	(void)arg;
	
	struct rope *rope;
	struct pole *pole;
	int index = MRAND_S % NROPES;

	kprintf("Marigold thread starting\n");

	while (ropes_left > 0){
		index = getNext(index, FRAND_S);
		pole = poles[index];
		rope = get_rope_from_pole(pole);						//Try to get rope. Fails if no rope (e.g cut by prince) or flower kileer at pole
																//Atomic: will release lock upon completion
																//Fast lock: no printing statements
		if (rope != NULL){
			kprintf("Marigold severed rope %d from stake %d\n", rope->num, pole->num);
			cut(rope);											//Cut then release rope
		}
	}
	kprintf("Marigold thread done\n");
	V(threads_sem);
}

static
void
flowerkiller(void *p, unsigned long arg)
{
	(void)p;
	(void)arg;
	
	struct rope *rope;
	struct pole *pole;
	int index = FRAND_S % NROPES;

	kprintf("Lord FlowerKiller thread starting\n");

	while (ropes_left > 0){
		index = getNext(index, FRAND_S);
		pole = poles[index];
		rope = get_rope_from_pole(pole);				//same action as marigold

		if (rope != NULL){
			while (!place_rope(rope, poles[index], pole)){				//Try to place rope somewhere marigold isn't around
																		//Before announcing THEN releasing the rope
				index = getNext(index, FRAND_S);
			}
		}
	}
	kprintf("Lord FlowerKiller thread done\n");
	V(threads_sem);
}

static
void
balloon(void *p, unsigned long arg)
{
	(void)p;
	(void)arg;
	
	kprintf("Balloon thread starting\n");
	P(bal_sem);															//Wait for the prince to announce.
	kprintf("Balloon freed and Prince Dandelion escapes!\n");
	kprintf("Balloon thread done\n");
	V(threads_sem);
}


// Change this function as necessary
int
airballoon(int nargs, char **args)
{

	int err = 0;
	int i = 0;

	(void)nargs;
	(void)args;
	(void)ropes_left;

	bal_sem = sem_create("balloon sem", 0);				//Balloon sleep here
	threads_sem = sem_create("thread sem", 0);			//Main sleep here
	rope_lock = lock_create("rope lock");				//Protect rope left count

	for (; i < NROPES; i++){
		ropes[i] = rope_create(i);
		poles[i] = pole_create(i, ropes[i]);	//Generating poles and ropes
	}

	ropes_left = NROPES;

	err = thread_fork("Marigold Thread",
			  NULL, marigold, NULL, 0);
	if(err)
		goto panic;
	
	err = thread_fork("Dandelion Thread",
			  NULL, dandelion, NULL, 0);
	if(err)
		goto panic;
	
	err = thread_fork("Lord FlowerKiller Thread",
			  NULL, flowerkiller, NULL, 0);
	if(err)
		goto panic;

	err = thread_fork("Air Balloon",
			  NULL, balloon, NULL, 0);
	if(err)
		goto panic;

	goto done;
panic:
	panic("airballoon: thread_fork failed: %s)\n",
	      strerror(err));
	
done:

	P(threads_sem);
	P(threads_sem);
	P(threads_sem);
	P(threads_sem);

	for (i = 0; i < NROPES; i++){		//Clean up
		rope_destroy(ropes[i]);
		pole_destroy(poles[i]);
	}

	sem_destroy(bal_sem);
	sem_destroy(threads_sem);
	lock_destroy(rope_lock);

	kprintf("Main thread done\n");
	return 0;
}
