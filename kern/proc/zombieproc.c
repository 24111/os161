#include <types.h>
#include <proc.h>
#include <synch.h>

struct zombie_proc *zombie_proc_create(){
	struct zombie_proc *ret;

	ret = kmalloc(sizeof(struct zombie_proc));
	if (ret == NULL)
		return NULL;

	ret->sem = sem_create("zomproc", 0);
	if (ret->sem == NULL){
		kfree(ret);
		return NULL;
	}

	ret->orphan = false;
	ret->exit = false;
	spinlock_init(&ret->lock);

	return ret;
}

/* Should not be called externally, except on err. Returns held value */
/* zombie will be destroyed by the parent on dead child */
/* or on waitpid */
/* or by exiting orphans */
int zombie_proc_destroy(struct zombie_proc *zproc){
	KASSERT(zproc != NULL);

	int retval = zproc->exit_code;
	sem_destroy(zproc->sem);
	spinlock_cleanup(&zproc->lock);
	kfree(zproc);
	return retval;
}

/* Return 1 to signal the wrapper to clean up the pid */
int zombie_proc_make_orphan(struct zombie_proc *zproc){
	spinlock_acquire(&zproc->lock);
	//Child exited. Clean up and move on.
		if (zproc->exit){
			spinlock_release (&zproc->lock);
			zombie_proc_destroy(zproc);
			return 1;
		}
	//Otherwise, inform the bad new
		else{
			zproc->orphan = true;
		}
	spinlock_release(&zproc->lock);
	return 0;
}

/* Return 1 to signal the child to clean up the pid */
int zombie_proc_exit(struct zombie_proc *zproc, int code){
	/* Special processes created from kernel do not have a zombie */
	if (zproc == NULL)
		return 0;

	spinlock_acquire(&zproc->lock);
	//Child orphan. Handles itself
		if (zproc->orphan){
			spinlock_release (&zproc->lock);
			zombie_proc_destroy(zproc);
			return 1;
		}
	//Otherwise, exit
		else{
			zproc->exit_code = code;	//The child's exit code
			V(zproc->sem);				//Tell the caring parent that their child has died (parent on waitpid)
			zproc->exit = true;			//Tell the uncaring parent that their child has died
		}
	spinlock_release(&zproc->lock);
	return 0;
}

int zombie_proc_wait(struct zombie_proc *zproc){
	P(zproc->sem);						//Wait for the child to exit
	return zombie_proc_destroy(zproc);
}

struct zproc_wrapper *zproc_wrapper_create(struct zombie_proc *zproc, pid_t pid){
	struct zproc_wrapper *ret;

	ret = kmalloc(sizeof(struct zproc_wrapper));
	if (ret == NULL)
		return NULL;

	ret->zproc = zproc;
	ret->pid = pid;
	ret->next = NULL;
	return ret;
}

void zproc_wrapper_destroy(struct zproc_wrapper *zwrapper){
	KASSERT(zwrapper != NULL);
	kfree(zwrapper);
}

void zproc_wrapper_append(struct zproc_wrapper *new, struct zproc_wrapper *old){
	new->next = old;
}

struct zproc_wrapper *zproc_wrapper_seek(struct zproc_wrapper *root, pid_t pid){
	while (root != NULL){
		if (root->pid == pid)
			return root;
		root = root->next;
	}
	return root;
}

void zproc_wrapper_orphan_maker(struct zproc_wrapper *zwrapper){
	struct zproc_wrapper *orphan;
	while (zwrapper != NULL){
		if (zwrapper->zproc != NULL){
			/* If child has exited, clean its pid */
			if(zombie_proc_make_orphan(zwrapper->zproc))
				pid_free(zwrapper->pid);
		}
		orphan = zwrapper;	//We have a new orphan wrapper!
		zwrapper = zwrapper->next;
		zproc_wrapper_destroy(orphan);	//Except it doesn't make sense (wrapper is used by parent), so we destroy it
	}
}