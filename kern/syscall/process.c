#include <syscall.h>
#include <types.h>
#include <current.h>
#include <proc.h>
#include <limits.h>
#include <kern/errno.h>
#include <addrspace.h>
#include <copyinout.h>
#include <filetable.h>
#include <vfs.h>
#include <kern/fcntl.h>
#include <kern/wait.h>
#include <mips/trapframe.h>
#include <lib.h>
#include <coremap.h>

#define EXECV_PAGE_SIZE 1024
#define EXECV_ARGV_PER_PAGE (EXECV_PAGE_SIZE/sizeof(char *))
#define EXECV_PAGE_COUNT (ARG_MAX/EXECV_PAGE_SIZE)

/* To implement a two-part copyin/out 
   We first copy the large aligned parts
   Then we copy the free part if it exist */

/* ====================================== STATIC METHODS ====================================== */
static void fork_wrapper(void *ptr, unsigned long nargs){
	(void) nargs;
	enter_forked_process((struct trapframe *)ptr);
}

static void execv_buf_cleanup(char** kbuf, size_t arg_size){
	int i = 0;
	while (arg_size > EXECV_PAGE_SIZE){
		kfree(kbuf[i++]);
		arg_size-=EXECV_PAGE_SIZE;
	}
	/* Last remaining page */
	if (arg_size > 0)
		kfree(kbuf[i]);
	kfree(kbuf);
}

/* Copy from userspace to kernel */
static int execv_copy_arg(char** args, char** kbuf, int *argc, size_t *arg_size){
	int i;
	/* General kbuf data */
	int page = 0;			//Next page index
	size_t rem_buf = ARG_MAX;
	/* Copy transaction data */
	userptr_t write_ptr;	//Write loc on kbuf
	userptr_t read_ptr;		//Read loc in userspace
	size_t rem_page = 0;		//Remaining space in page
	size_t copyin_size;		//Data copied in

	int result;

	*arg_size = 0;
	*argc = 0;
	/* Copy in the argument, one by one */
	read_ptr = (userptr_t) args;
	do{
		/* Next page logic */
		if (rem_page == 0){
			if (rem_buf == 0)
				return E2BIG;
			kbuf[page] = kmalloc(EXECV_PAGE_SIZE);
			if (kbuf[page] == NULL)
				return ENOMEM;
			write_ptr = (userptr_t) kbuf[page++];
			rem_page = EXECV_PAGE_SIZE;
		}

		/* Copy in logic */
		result = copyin((const_userptr_t) read_ptr, write_ptr, sizeof(char *));
		if (result){
			if (rem_page == EXECV_PAGE_SIZE)
				kfree(kbuf[page - 1]);		//Page not used at all, dealloc it
			return result;
		}

		/* Offset adjustment */
		(*argc)++;
		*arg_size += sizeof(char *);
		write_ptr += sizeof(char *);
		read_ptr  += sizeof(char *);
		rem_page  -= sizeof(char *);
		rem_buf   -= sizeof(char *);
	} while (*((char**)(write_ptr - sizeof(char *))) != NULL);	//We increamented the top pointer, move it back
	
	/* NULL pointer */
	(*argc)--;

	/* Now copy in the data */
	for (i = 0; i < *argc; i++){
		/* Essentially we find the page and index of the char* from the kbuf */
		read_ptr = (userptr_t)((char**) kbuf[i / EXECV_ARGV_PER_PAGE])[i % EXECV_ARGV_PER_PAGE];
		
		/* Now, set the same address to the offset to stackptr */
		((char**) kbuf[i / EXECV_ARGV_PER_PAGE])[i % EXECV_ARGV_PER_PAGE] = (char *) *arg_size;
		/* Loop to copy */
		while (true){
			/* Next page logic */
			if (rem_page == 0){
				if (rem_buf == 0)
					return E2BIG;
				kbuf[page] = kmalloc(EXECV_PAGE_SIZE);
				if (kbuf[page] == NULL)
					return ENOMEM;
				write_ptr = (userptr_t) kbuf[page++];
				rem_page = EXECV_PAGE_SIZE;
			}

			result = copyinstr((const_userptr_t) read_ptr, (char *) write_ptr, rem_page, &copyin_size);
			
			/* Not done yet, try again */
			if (result == ENAMETOOLONG){
				*arg_size += rem_page;
				read_ptr += rem_page;
				rem_buf -= rem_page;
				rem_page = 0;
				continue;
			} else if (result){
				if (rem_page == EXECV_PAGE_SIZE)
					kfree(kbuf[page - 1]);
				return result;
			}
			/* Stack alignment */
			copyin_size += 3;
			copyin_size &= ~3;

			*arg_size += copyin_size;
			write_ptr += copyin_size;
			rem_page  -= copyin_size;
			rem_buf   -= copyin_size;
			break;
		}
	}
	return 0;
}

/**
  *	Load argv from kernel buffer
  * Convert the offset into virtual address on stack
  */
/* Load args to stack pointer */
static int execv_load_arg(int argc, size_t arg_size, char** kbuf, userptr_t *argv, vaddr_t *stackptr){
	int i;
	int result;
	size_t offset = 0;

	KASSERT((arg_size % 4) == 0);
	KASSERT(arg_size <= ARG_MAX);

	/* Adjust the stack pointer */
	*stackptr -= arg_size;

	/* Convert offset to actual address */
	for (i = 0; i < argc; i++){
		((char**) kbuf[i / EXECV_ARGV_PER_PAGE])[i % EXECV_ARGV_PER_PAGE] += *stackptr;
	}

	/* Fragment copy */
	i = 0;		//Page counter
	while (arg_size > EXECV_PAGE_SIZE){
		result = copyout((const void *) kbuf[i++], (userptr_t) *stackptr + offset, EXECV_PAGE_SIZE);
		if (result)
			return result;
		arg_size-= EXECV_PAGE_SIZE;
		offset+= EXECV_PAGE_SIZE;
	}

	/* If not aligned */
	if (arg_size > 0){	
		result = copyout((const void *) kbuf[i], (userptr_t) *stackptr + offset, arg_size);
		if (result)
			return result;
	}

	*argv = (userptr_t) *stackptr;
	return 0;
}


void sys_getpid(uint64_t *retval){
	*retval = curproc->p_pid;
}

int sys_fork(struct trapframe *tf, uint64_t *retval){
	int result;
	struct proc *proc;
	struct zproc_wrapper *zwrapper;
	struct addrspace *cur_as;
	struct trapframe *tfc;			//Copied trapframe to heap
	struct zombie_proc *zproc;

	tfc = kmalloc(sizeof (struct trapframe));
	if (tfc == NULL)
		return ENOMEM;
	memcpy(tfc, tf, sizeof (struct trapframe));

	/* Create a blank new process. The new process should have a unique new PID already */
	proc = proc_create_runprogram(curproc->p_name);
	if (proc == NULL){
		kfree(tfc);
		return ENOMEM;
	}

/* ============================= CLONING ==========================*/

	/* ====================== VM ======================= */
		cur_as = proc_getas();
		/* We should be in a process. */
		KASSERT(cur_as != NULL);
		/* Copy the address space. */
		result = as_copy(cur_as, &proc->p_addrspace);
		if (result) {
			kfree(tfc);
			proc_destroy(proc);
			return result;
		}
	/* =================== Assign a PID ================= */
		result = pid_get_new(&proc->p_pid);
		if (result){
			kfree(tfc);
			proc_destroy(proc);
			return result;
		}
	/* ================= ZOMBIES(VM) ==================== */
		/* Init the zombie */
		zproc = zombie_proc_create();
		if (zproc == NULL){
			kfree(tfc);
			pid_free(proc->p_pid);
			proc_destroy(proc);
			return ENOMEM;
		}
		/* Create a wrapper for the parent process */
		zwrapper = zproc_wrapper_create(zproc, proc->p_pid);
		if (zwrapper == NULL){
			kfree(tfc);
			pid_free(proc->p_pid);
			zombie_proc_destroy(zproc);
			proc_destroy(proc);
			return ENOMEM;
		}
		/* Add it to the list of childrens */
		zproc_wrapper_append(zwrapper, curproc->p_children);
		curproc->p_children = zwrapper;
		proc->p_zombie = zproc;

	/* =================== VFS ========================== */
		/* fork the fdt over */
		fdt_fork(curproc->p_fdt, proc->p_fdt);

	*retval = proc->p_pid;
/* =========================== FORKING ============================ */
	//Fork the child process
	result = thread_fork(curproc->p_name,
	    proc,
	    fork_wrapper,
	    (void*)(tfc), 1);


	if (result){
		kfree(tfc);
		pid_free(proc->p_pid);
		curproc->p_children = zwrapper->next;
		zproc_wrapper_destroy(zwrapper);
		zombie_proc_destroy(proc->p_zombie);
		proc_destroy(proc);
		return result;
	}
	return 0;
}

int sys_execv(const char *program, char **args){
	/* Address space fields */
	struct addrspace *as;
	struct addrspace *oldas;
	/* Vnode */
	char buf[PATH_MAX + 1];
	struct vnode *v;
	/* Argv and stacks */
	char **kbuf;	//Kernel buffer
	vaddr_t entrypoint, stackptr;
	size_t arg_size;
	userptr_t argv;
	int argc;
	/* Fault */
	int result;

	kbuf = kmalloc(sizeof(char*) * EXECV_PAGE_COUNT);
	if (kbuf == NULL)
		return ENOMEM;

/* ============================== LOAD PROGRAM ================================= */
	/* ================= Load argv into kernel ================= */
		/* Copyin the data */
		result = execv_copy_arg(args, kbuf, &argc, &arg_size);
		if (result){
			execv_buf_cleanup(kbuf, arg_size);
			return result;
		}
	/* ================= OPEN FILE ================= */
		/* Copy in the suppied progname */
		result = copyinstr((const_userptr_t) program, buf, PATH_MAX, NULL);
		if (result){
			execv_buf_cleanup(kbuf, arg_size);
			return result;
		}
		/* Open the file. */
		result = vfs_open(buf, O_RDONLY, 0, &v);
		if (result){
			execv_buf_cleanup(kbuf, arg_size);		
			return result;
		} 
	/* =============== AS INIT ===================== */
		/* Create a new address space. */
		as = as_create();
		if (as == NULL) {
			execv_buf_cleanup(kbuf, arg_size);
			vfs_close(v);
			return ENOMEM;
		}

		/* Switch to it and activate it. */
		oldas = proc_setas(as);
		as_activate();

	/* =============== LOAD EXECUTABLE ============= */
		/* Load the executable. */
		result = load_elf(v, &entrypoint);
		if (result) {
			execv_buf_cleanup(kbuf, arg_size);
			/* Revert the address space */
			proc_setas(oldas);
			as_activate();
			as_destroy(as);
			/* Close the vfs */
			vfs_close(v);
			return result;
		}

		/* Done with the file now. */
		vfs_close(v);

/* ================================ RUN PROGRAM ================================= */
	/* Define the user stack in the address space */
	result = as_define_stack(as, &stackptr);
	if (result) {		
		execv_buf_cleanup(kbuf, arg_size);
		/* Revert the address space */
		proc_setas(oldas);
		as_activate();
		as_destroy(as);
		return result;
	}
	/* Load the variables from kbuf */
	result = execv_load_arg(argc, arg_size, kbuf, &argv, &stackptr);
	if (result){
		execv_buf_cleanup(kbuf, arg_size);	
		/* Revert the address space */
		proc_setas(oldas);
		as_activate();
		as_destroy(as);
		return result;
	}

	execv_buf_cleanup(kbuf, arg_size);
	as_destroy(oldas);
	/* Warp to user mode. */
	enter_new_process(argc /*argc*/, argv /*userspace addr of argv*/,
			  NULL /*userspace addr of environment*/,
			  stackptr, entrypoint);

	/* enter_new_process does not return. */
	panic("enter_new_process returned\n");
	return EINVAL;
}

int sys_waitpid(pid_t pid, int *status, int options, uint64_t *retval){
	int exitcode;
	int result;
	struct zproc_wrapper *zwrapper;


	if (options)
		return EINVAL;													//Unused

	if (status != NULL){
		/* Verify memloc. Since waiting for the result will take time */
		/* We want to assure that status is valid */
		result = copyin((const_userptr_t) status, &exitcode, 1);		//Verify memory location
		if (result)
			return result;
	}

	zwrapper = zproc_wrapper_seek(curproc->p_children, pid);
	if (zwrapper == NULL)
		return ECHILD;

	if (zwrapper->zproc == NULL)
		return ESRCH;

	/* This will destroy zproc */
	exitcode = zombie_proc_wait(zwrapper->zproc);
	/* Writeback? */
	if (status != NULL){
		result = copyout(&exitcode, (userptr_t) status, sizeof(int));				//We confirmed the pointer is valid
		/* Critical error for current structure. The user shouldn't be able to trigger this */
		if (result)		/* To warn possible code issues */													
			panic ("copyout failed in sys_waitpid\n");
	}

	/* Mark child as exited */
	zwrapper->zproc = NULL;
	*retval = (uint64_t) pid;								//Return the pid
	return 0;
}

void sys__exit(int exitcode){
	struct proc *proc = curproc;
	/* Clean up if parent has exited */
	if (zombie_proc_exit(curproc->p_zombie, _MKWAIT_EXIT(exitcode)))
		pid_free(curproc->p_pid);
	proc_remthread(curthread);
	//We're cleaning after ourself, but it's seen as if it's someone else
	//So, flush the tlb first
	coremap_flushtlb();
	proc_destroy(proc);									//This does not clean up the zombie
	thread_terminate();											//Terminate current thread
}