#include <types.h>
#include <syscall.h>
#include <proc.h>
#include <addrspace.h>
#include <kern/errno.h>
#include <pagetable.h>
#include <vm.h>

int sys_sbrk(uint64_t *retval, intptr_t amount){
	struct addrspace *as;
	vaddr_t new, old;
	int result;
	as = proc_getas();

	//Not aligned?
	if((amount & PAGE_FRAME) != (unsigned) amount)
		return EINVAL;

	//Anti-overflow logic.
	if (amount > 0){
		//Can't get more that til the heap
		if ((unsigned) amount > (as->stackbot - as->heapptr))
			return ENOMEM;
		new = (unsigned) amount + as->heapptr; 
	} else {
		amount = -amount;
		if ((unsigned) amount > (as->heapptr - HEAP_START))
			return EINVAL;
		new = as->heapptr - (unsigned) amount;
	}

	old = as->heapptr;	
	//Unlike shrinking, we need to check if we have enough disk to grow
	if (new > old){
		while (old < new){
			result = pagetable_insert(as->pt, old);
			if (result){
				//We leak the mem a bit here, since I'm too lazy to revoke those pages
				//These will get reclaimed on process exit
				//VMfault would fail the request anyway, even if a pagetable entry exist
				return ENOMEM;
			}
			old+= PAGE_SIZE;
		}
	}
	*retval = as->heapptr;
	as->heapptr = new;
	return 0;
}