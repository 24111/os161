#include <types.h>
#include <syscall.h>
#include <lib.h>
#include <filetable.h>
#include <proc.h>
#include <vfs.h>
#include <uio.h>
#include <copyinout.h>
#include <kern/errno.h>
#include <limits.h>

int sys_open(const char* file, int flags, mode_t mode, uint64_t *retval){
	struct vnode *vn;
	struct filetable *ft;
	struct fdtable *fdt;
	char buf[PATH_MAX+1];
	size_t size;
	int result;

	result = copyinstr((const_userptr_t)file, buf, PATH_MAX, &size);
	if (result)
		return result;

	result = vfs_open(buf, flags, mode, &vn);
	if (result)
		return result;

	ft = filetable_create(vn, flags);
	fdt = proc_getfdt();
	result = fdtable_open(fdt, ft, retval);
	if (result)
		return result;
	return 0;
}

int sys_read(int fd, void *buf, size_t buflen, uint64_t *retval){
	struct filetable *ft;
	struct fdtable *fdt;
	int result;

	fdt = proc_getfdt();
	result = fdtable_getentry(fdt, &ft, fd);
	if (result)
		return result;

	result = filetable_read(ft, buf, buflen, retval);
	filetable_decref(ft);
	if (result)
		return result;
	return 0;
}

int sys_write(int fd, const void *buf, size_t nbytes, uint64_t *retval){
	struct filetable *ft;
	struct fdtable *fdt;
	int result;

	fdt = proc_getfdt();
	result = fdtable_getentry(fdt, &ft, fd);
	if (result)
		return result;

	result = filetable_write(ft, buf, nbytes, retval);
	filetable_decref(ft);
	if (result)
		return result;
	return 0;
}

int sys_close(int fd){
	struct fdtable *fdt;
	int result;

	fdt = proc_getfdt();
	result = fdtable_close(fdt, fd);
	if (result)
		return result;
	return 0;
}

int sys_lseek(int fd, off_t pos, int whence, uint64_t *retval){
	struct fdtable *fdt;
	struct filetable *ft;
	int result;

	fdt = proc_getfdt();
	result = fdtable_getentry(fdt, &ft, fd);
	if (result)
		return result;

	result = filetable_lseek(ft, pos, whence, retval);
	filetable_decref(ft);
	if (result)
		return result;
	return 0;
}

int sys_chdir(const char *pathname){
	size_t length;
	char buff[PATH_MAX+1];
	int result;

	result = copyinstr((const_userptr_t)pathname, buff, PATH_MAX, &length);		//Copy in the data
	if(result)
		return result;

	result = vfs_chdir(buff);	//Call vfs to do our work
	if (result)
		return result;
	return 0;
}

int sys_dup2(int oldfd, int newfd){
	struct fdtable *fdt;
	int result;

	fdt = proc_getfdt();
	result = fdtable_dup2(fdt, oldfd, newfd);
	if (result)
		return result;
	return 0;
}

int sys___getcwd(char *buf, size_t buflen, uint64_t *retval){
	int result;
	size_t length;
	struct iovec iov;
	struct uio u;
	char kbuf[buflen];		//Buffer within kernel space.

	uio_kinit(&iov, &u, kbuf, buflen, 0, UIO_READ);	//Init a kernel UIO for vfs
	result = vfs_getcwd(&u);	//Then we read the dir to uio. This can fail, and is why we need a kbuffer
	
	if (result)
		return result;

	length = u.uio_offset;		//retval
	result = copyout(kbuf, (userptr_t) buf, length);	//Try to copy data back
	if (result)
		return result;
	*retval = length;				//Cast to int64
	return 0;
}