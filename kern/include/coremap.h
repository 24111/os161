/*
 *	Coremap
 *	This is probably where the most complicated logics and concurrency resides
 *	Coremap functions handles physical page allocation and swappings
 *	Handles TLB and MMU
 *  Keep tract of MMU
 *	Note that since kseg1 is mapped to the first 512 MB of memory
 *	And OS161 have a hard max RAM size of 512 MB
 *	I don't wanna bother with splitting into two regions (One that allows kmalloc and one that doesn't)
 */

/*
 *	DOES NOT IMPLEMENT MEMORY SHARING IN ANY WAY
 *	I got lazy. Memory sharing requires additional information being stored
 *	That interferes with current model.
 *	Tradeoff between speedy fork, memory shares for simplicy
 */

/*
 *	Locking logic:
 *	To allocate memory: lock it. Other cpu might be allocating at the same time.
 *	To look up information for tlb: lock it. Other cpu might evict your page and call a tlb shootdown
 *	To rewrite TLB: just disable interrupt. Other cpu will call a shootdown (if memory is filled)
 *	So you'd end up rewriting the TLB, but at least your tlb is clean.
 *	Essentially: Always lock it. Memory allocation requires scanning the entire coremap, highly unsafe
 *	Tlb shootdown will force you to refresh the tlb. However, if you need to load a page from disc
 *	You would still need to lock the coremap to do so anyway.
 *	Since pagetables do not have locks, if another cpu tries to evict your page while you're reading from it
 *	You may read info on an evicted page and tries to bring it back. Which you should, except that
 *	The concurrency behind the mess is ridiculous. Pagetables should be small enough that seek is fast anyway.
 */

/*
 *	Since I do not have a clean up thread, I used another logic for paging
 *  First, processes do not try to shoot eachother up
 *  If it's in another CPU's tlb, just give up. Inuse page marks if it's in a tlb
 *	I mean, you can just try to find one not being mapped in a tlb?
 *  End result being that I don't have to do TLB shootdown.
 *	Originally I planned to do so (even with a disk daemon!)
 *	But since tests are for very small memory, no point in some complicated scheme. We're more likely to thrash and shootout
 *	Same thing for kmalloc. No point in making more threads if the current ones are killing eachother for memory
 *	So don't be USA and ban guns!
 *	Play nice!
 */

/*
 *	EFFICIENCY OF DIRTY BIT USAGE
 *	Dirty bit allows kernel to snatch pages without swapping
 *	However, if every page is dirty, it's useless!
 *	So, when is a page BOTH clean and is not in use?
 *	Loading code data dirties the page
 *	Stack is always dirty
 *	Seems like if it goes to user, it's dirty!
 *	But, user also cleans up dirty pages (on their own)
 *	So it goes -> Dirty -> Disk -> Loaded(clean/reclaimable)
 *	Long lived pages (code segment) will almost always be clean after the initial load
 *	Same for memory region not often modified
 *	We can boost efficiency by having a cleaner thread just picking up these things periodically!
 *	(Just sayin, I don't actually have a cleaner thread. It's easy to implement, but I don't wanna spend more time debugging)
 *	Cleaning up on tlb_flush or unload is long and complicated, as it requires releasing the lock. You can free at most a page at a time
 *	by modifying the reserved bit. Adding a cleaning thread would be easier than reason around releasing coremap lock
 */
#include <spinlock.h>

/*
 * We have 19 bits for kmalloc size and pagetable indexes. This matches all user/kernel paging needs
 * Note that this does not represent virtual memory address. There exist a 1-to-1 translation between the two
 * But they are not the same
 */
#define INDEX_MASK 0x7ffff000		//Kernel malloc size / pagetable index. Leave 12 signal bits. 3 is currently in use.
#define RESERVED 8 //Reserved bit is for waiting on IO operations
#define DIRTY 4	//Dirty bit. Dirty pages cannot be *evicted*
#define INUSE 2	//In use bit
#define VALID 1
#define KERNEL 0x80000000	//Indicates kernel memory. A bit redundant, as it behave the same as VALID | INUSE
#define CM_SHIFT 12		//Amount of shifting to get rid of signal bits

struct coremap {
	size_t memsize;
	//First physical base address. paddr is calculated = pbase + (index << 12)
	paddr_t pbase;
	/* tag = Kernel bit (1) - data field(19) - flag field(12) */
	uint32_t *tags;
	/* This is to reference to the associated pagetables to look up data */
	/* Reduce seek time */
	struct pagetable **ptentries;
	struct spinlock lock;
};

//Creates a kernel entry
#define KENTRY(index) (((index) << CM_SHIFT) | KERNEL | VALID | INUSE)
//Extracts kmalloc length (for kfree) OR pagetable index (for user page)
#define CM_DAT(data)  (((data) & (~KERNEL)) >> CM_SHIFT)
/* Coremap launch */
void coremap_bootstrap(void);

/* Peekabo from other structures that may want to know memory */
size_t coremap_getmemsize(void);

//Allocate some free physical pages, swapping if neccessary
//Used by kmalloc. Will return a kseg0 address
vaddr_t coremap_getppages(size_t size);

void coremap_freeppages(vaddr_t vaddr);

/*
 * Called by VM system to allocate a user space
 * Handles VM allocation and VM faults
 * On VM fault: look up pagetable
 * On look up miss: add new entry
 */
int coremap_allocvpage(vaddr_t vpage, struct pagetable *pt, int flag);

/*
 *	Revoke all memory assigned to a process (pagetable).
 *	Called on address space destroy
 */
void coremap_freevpages(struct pagetable *pt);

int coremap_forkmem(struct pagetable *new, struct pagetable *old);

//On context switch. Clears the inuse bit
void coremap_flushtlb(void);
