/**
  *	Contiguous disc chunk to handle page swapping
  *	Uses a bitmap to allocate
  * Each swapped entry is given an index to align offset
  */

#define SWAP_SPACE 1280

struct pageswap {
	struct bitmap *bm;
	struct vnode *disk;
	size_t size;
};

// There should be only one instance of pageswap. Shutting down pageswap is kinda not needed
// As the system is shutting down anyway
void pageswap_init (void);

//As we are working with < 512MB, kernel can access all these without restriction, providing that we supply kernel addresses
void pageswap_store (vaddr_t addr ,uint32_t index);

void pageswap_load  (vaddr_t addr ,uint32_t index);

int pageswap_malloc (uint32_t *index);

//Free a page. This happens when a process is destroyed
void pageswap_free  (uint32_t index);

