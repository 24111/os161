/*
 * Copyright (c) 2013
 *	The President and Fellows of Harvard College.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE UNIVERSITY AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE UNIVERSITY OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#ifndef _PROC_H_
#define _PROC_H_

/*
 * Definition of a process.
 *
 * Note: curproc is defined by <current.h>.
 */

#include <spinlock.h>
#include <thread.h> /* required for struct threadarray */

struct addrspace;
struct vnode;

/*
 * Process structure.
 */
struct proc {
	char *p_name;			/* Name of this process */
	struct spinlock p_lock;		/* Lock for this structure */
	struct threadarray p_threads;	/* Threads in this process */
	pid_t p_pid;		/* PID of this process */

	/* VM */
	struct addrspace *p_addrspace;	/* virtual address space */
	struct zproc_wrapper *p_children;	/* Tracker for childrens */
	struct zombie_proc *p_zombie;	/* Not yet walking zombie */

	/* VFS */
	struct vnode *p_cwd;		/* current working directory */
	struct fdtable *p_fdt;

	/* add more material here as needed */
};

/* This is the process structure for the kernel and for kernel-only threads. */
extern struct proc *kproc;

/* Call once during system startup to allocate data structures. */
void proc_bootstrap(void);

/* Create a fresh process for use by runprogram(). */
struct proc *proc_create_runprogram(const char *name);

/* Destroy a process. */
void proc_destroy(struct proc *proc);

/* Attach a thread to a process. Must not already have a process. */
int proc_addthread(struct proc *proc, struct thread *t);

/* Detach a thread from its process. */
void proc_remthread(struct thread *t);

/* Fetch the address space of the current process. */
struct addrspace *proc_getas(void);

/* Change the address space of the current process, and return the old one. */
struct addrspace *proc_setas(struct addrspace *);

/* Fetch the file descriptor table of the current process. */
struct fdtable *proc_getfdt(void);

/* Fetch the processID */
pid_t proc_getpid(void);

/*==================== PID ================================================*/
struct pid_table{
	struct bitmap *pid_bitmap;

	struct spinlock pid_lock;	/* Protects the bitmap */
};

/* Called by kernel thread to initialize pid table */
void pid_table_bootstrap(void);

/* Destroy PID table */
void kpid_table_destroy (void);

/* Get a new unused PID */
int pid_get_new(pid_t *pid) ;

/* Free a pid */
void pid_free(pid_t pid);


/*========================ZOM PROC================*/
/*
 *	Holds the return value for waitpid
 *	Is cleaned up by parents and orphans
 */
struct zombie_proc {
	struct spinlock lock;
	struct semaphore *sem;
	bool orphan;
	bool exit;
	int exit_code;
};

//Called within a proc_create
struct zombie_proc *zombie_proc_create(void);

int zombie_proc_destroy(struct zombie_proc *zproc);

//Mark this zombie as orphan. Orphans reap themselves.
//Called by exiting parent on every child processes
int zombie_proc_make_orphan(struct zombie_proc *zproc);

//Called by the child process on exiting. Return 1 if pid self clean up is needed
int zombie_proc_exit(struct zombie_proc *zproc, int code);

//Called by the parent
int zombie_proc_wait(struct zombie_proc *zproc);

/*==========================Wrapper for zproc========================*/
struct zproc_wrapper{
	struct zombie_proc *zproc;
	pid_t pid;
	struct zproc_wrapper *next;
};

struct zproc_wrapper *zproc_wrapper_create(struct zombie_proc *zproc, pid_t pid);

void zproc_wrapper_destroy(struct zproc_wrapper *zwrapper);

void zproc_wrapper_append(struct zproc_wrapper *new, struct zproc_wrapper *old);

struct zproc_wrapper *zproc_wrapper_seek(struct zproc_wrapper *root, pid_t pid);

void zproc_wrapper_orphan_maker(struct zproc_wrapper *zwrapper);
#endif /* _PROC_H_ */