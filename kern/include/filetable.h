#include <spinlock.h>
#include <uio.h>

#define FDT_SIZE __OPEN_MAX


/*
 *	A file table entry
 */
struct filetable{
	struct vnode *vn;
	int flags;
	volatile off_t offset;
	volatile uint32_t ref_c;
	struct spinlock c_lock;		//Shared counter lock 
	struct lock *rw_lock;	//Lock over rw operation
};

/*
 *	The process file descriptor table
 */
struct fdtable{
	struct filetable **ft_ptr;
	volatile uint32_t in_use;	// To check if fildes is in use, check (in_use & (1 << fildes))
	struct spinlock t_lock;		//Lock for table
};


struct fdtable *fdtable_create(void);		//Create and return a blank fdt
void fdtable_destroy(struct fdtable*);

/*
 *	A new filetable entry is not directly linked to any fdtable!
 *	This method must only be called inside kernel followed up by
 *  fdtable_open with the corresponding fdt
 *  fdtable_open will delete the filetable on fail
 */
struct filetable *filetable_create(struct vnode *, int flags);
int fdtable_init(struct fdtable *);
void filetable_destroy(struct filetable *);

/*
 * Links a ft to a fdt entry. fd is recorded in retval. Retval is not modified in
 * the event of an error
 */
int fdtable_open(struct fdtable *fdt, struct filetable *, uint64_t *retval);

/*
 *	Duplicate fd to fd2. Calls fdtable_close on fd2 then manually assign a new fd2 entry
 */
int fdtable_dup2(struct fdtable *fdt, int fd, int fd2);	

/*
 *	Close the specified fd, return EBADF if fd is not opened
 *	Calls decref on the associated ft
 */
int fdtable_close(struct fdtable *fdt, int fd);	

/*
 *	Get the filetable for reading and writing purpose
 *	Atomic reading a ft entry. Guarantees if at time of reading, fd is valid, operation is valid
 *	Increment reference count to ensure ft do not get closed during the operation
 */
int fdtable_getentry(struct fdtable *fdt, struct filetable **ret, int fd);

/*
 *	Similar to vnode, but on filetable!
 *	It will close AND delete the filetable if count reach 0
 *	No atomic guaranteed. To ensure safety against race cond
 *	ALWAYS increase the ref count when you are working on it!
 *	This is to ensure if the fd is shared across threads
 *	A reasonable behavior is ensured
 */
int filetable_decref(struct filetable *);

void filetable_incref(struct filetable *);

int filetable_read(struct filetable *, void *buf, size_t buflen, uint64_t *retval);
int filetable_write(struct filetable *, const void *buf, size_t buflen, uint64_t *retval);
int filetable_lseek(struct filetable *, off_t pos, int whence, uint64_t *retval);
void filetable_uio_init(struct iovec *, struct uio *,
	  void *buf, size_t len, off_t pos, enum uio_rw rw);

/* Helper for fork */
void fdt_fork(struct fdtable *old, struct fdtable *new);