/*
	PAGE TABLE
	WRITTEN BY LAM NGUYEN
	It is a dynamic array that doubles in side everytime limit is reached
	Last bit indicate availability (1 = in mem).
	Protected by coremap lock on modify after creation
	Argument for such coarse locking mechanism:
	Pagetable is a LUT to store information
	It doesn't make sense to read/modify the pagetable without modifying the coremap. You're either allocating
	memory or swapping pages
*/

#define PAGETABLE_MIN 4

/*
 *	TOWED: The page you're looking for is dirty, being given to another proc, and has yet to arrive at the pound (disk)
 *	VALID: invalid page can be given as any ppage. It will probably gets loaded and trigger a VM_FAULT, flagging it as dirty.
 *  INMEM: Memory is in mem. Look it up and update the tlb
 */
#define PT_TOWED 4
#define PT_VALID 2
#define PT_INMEM 1

struct pagetable {
	size_t size;
	vaddr_t *vaddrs;
	/* Index of the memory location */
	/* Last bit indicates memory availability */
	/* Second-last bit indicates valid */
	/* Used by coremap to store datas */
	uint32_t *indexes;
};

struct pagetable *pagetable_create (void);

//To be called inside core, after it has cleared the data
void pagetable_destroy (struct pagetable *pt);

//The reason we don't just let the coremap handles the insertion is due to resizing logic
int pagetable_insert (struct pagetable *pt, vaddr_t vaddr);

struct pagetable *pagetable_copy (struct pagetable *pt);

/*
 *	INMEM: Do not need to copy over
 *	VALID: Uninitiated but declared
 *  INIT: Invalid entry with reserved disk location
 *	TOWED: Entry is towed
 */

#define PAGETABLE_INMEM(entry) (((entry) & 1))
#define PAGETABLE_VALID(entry) ((entry) & 2)
#define PAGETABLE_INDEX(entry) ((entry) >> 3)
#define PAGETABLE_INIT(index) ((index) << 3)
#define PAGETABLE_TOWED(entry) ((entry) & PT_TOWED)