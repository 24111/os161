#include <types.h>
#include <lib.h>
#include <filetable.h>
#include <proc.h>
#include <vfs.h>
#include <uio.h>
#include <spinlock.h>
#include <vnode.h>
#include <kern/errno.h>
#include <synch.h>
#include <limits.h>
#include <kern/stat.h>
#include <kern/seek.h>
#include <kern/fcntl.h>

struct filetable *filetable_create(struct vnode *vn, int flags){
	struct filetable *ft;

	ft = kmalloc(sizeof(struct filetable));
	if (ft == NULL){
		return NULL;
	}

	spinlock_init(&ft->c_lock);
	ft->rw_lock = lock_create("");
	ft->offset = 0;
	ft->ref_c = 1;
	ft->vn = vn;
	ft->flags = flags;
	return ft;
}

void filetable_destroy(struct filetable *ft){
	KASSERT(ft != NULL);
	KASSERT(ft->ref_c == 1);		//Don't destroy ft in use!
	KASSERT(spinlock_do_i_hold(&ft->c_lock));	//Must be called from decref

	spinlock_release(&ft->c_lock);
	VOP_DECREF(ft->vn);	
	spinlock_cleanup(&ft->c_lock);
	lock_destroy(ft->rw_lock);
	kfree(ft);
}

struct fdtable *fdtable_create(void){
	struct fdtable *fdt;

	fdt = kmalloc(sizeof(struct fdtable));
	if (fdt == NULL){
		return NULL;
	}

	fdt->ft_ptr = kmalloc(FDT_SIZE * sizeof(userptr_t));
	if (fdt->ft_ptr == NULL){
		kfree(fdt);
		return NULL;
	}

	fdt->in_use = 0;

	spinlock_init(&fdt->t_lock);
	return fdt;

}

/*
 *	Helper func 
 */
int fdtable_init(struct fdtable *fdt){
	int result;
	uint64_t open = -1;
	char stdin_char[] = "con:";
	char stdout_char[] = "con:";
	char stderr_char[] = "con:";

/*
 *	On fdtable init, open up stdin, stdout and stderr
 */
	struct filetable *stdin_ft;
	struct filetable *stdout_ft;
	struct filetable *stderr_ft;

	struct vnode *stdin_vn;
	struct vnode *stdout_vn;
	struct vnode *stderr_vn;

//Open the vfs vnode layer 
	result = vfs_open(stdin_char, O_RDONLY, 0, &stdin_vn);
	if (result)
		return result;
	result = vfs_open(stdout_char, O_WRONLY, 0, &stdout_vn);
	if (result)
		return result;
	result = vfs_open(stderr_char, O_WRONLY, 0, &stderr_vn);
	if (result)
		return result;

//Create the filetable entries
	stdin_ft = filetable_create(stdin_vn, O_RDONLY);
	stdout_ft = filetable_create(stdout_vn, O_WRONLY);
	stderr_ft = filetable_create(stderr_vn, O_WRONLY);

//Linking the ft entry to the fdtable
	fdtable_open(fdt, stdin_ft, &open);	//Take advantage of the fact that we know open does not modify result on error
	KASSERT(open == 0);	//Should be linking with 0, we are in init!
	fdtable_open(fdt, stdout_ft, &open);
	KASSERT(open == 1);
	fdtable_open(fdt, stderr_ft, &open);
	KASSERT(open == 2);

	return 0;			
}

void fdtable_destroy(struct fdtable* fdt){
	int count;
	KASSERT(fdt != NULL);
	
//Close all remaining file descriptors
		for (count = 0; fdt->in_use > 0; count++){
			if (fdt->in_use & (1 << count)){
				fdtable_close(fdt, count);
			}
		}

	kfree(fdt->ft_ptr);
	spinlock_cleanup(&fdt->t_lock);
	kfree(fdt);
}

int fdtable_open(struct fdtable *fdt, struct filetable *ft, uint64_t *retval){
	int count;

	spinlock_acquire(&fdt->t_lock);
	//Find an empty fd
		for (count = 0; count < FDT_SIZE; count++){
			if ((fdt->in_use & (1 << count)) == 0){
				*retval = count;
				break;
			}
		}
	//Found it, now we mark the entry as exist, and add the ft
		if (count < FDT_SIZE){
			fdt->in_use |= 1 << count;	//modify tracking list
			fdt->ft_ptr[count] = ft;	//modify corresponding ft pointer
		}
	spinlock_release(&fdt->t_lock);

	if (count == FDT_SIZE){
		filetable_decref(ft);
		return EMFILE;
	}
	return 0;
}

int fdtable_close(struct fdtable *fdt, int fd){
	struct filetable *ft;	//Once we release fdt, there's no guarantee that the field is not overwritten

	if (fd >= FDT_SIZE || fd < 0)	//Can only accept 0 to 31
		return EBADF;
//Lock the fdt to read inuse, and write if needed
	spinlock_acquire(&fdt->t_lock);
		if ((fdt->in_use & (1 << fd)) == 0){	
			spinlock_release(&fdt->t_lock);
			return EBADF;
		}
		else{
			fdt->in_use -= (1 << fd);	//Hacky solution to write a bit to 0
		}
		ft =  fdt->ft_ptr[fd];
	spinlock_release(&fdt->t_lock);
	/* The file is marked as decref on fdtable, safe to decref*/
	filetable_decref(ft);	//Reduce refcount, clean up if needed
	return 0;
}


int fdtable_dup2(struct fdtable *fdt, int fd, int fd2){
	struct filetable *ft, *oldft;
	int cleanup = 0;

	if (fd == fd2)
		return 0;
	if (fd >= FDT_SIZE || fd < 0 || fd2 >= FDT_SIZE || fd2 < 0)
		return EBADF;

	//Begin atomic dup process. Associated ft is protected by the fdt lock
	//In other word, ft cannot be deallocated since fd is still holding its refcount (and fd is protected)
	spinlock_acquire(&fdt->t_lock);
		if ((fdt->in_use & (1 << fd)) == 0){	//fd is not in use!
			spinlock_release(&fdt->t_lock);
			return EBADF;			
		}

		if (fdt->in_use & (1 << fd2)){			//Close fd2 if needed.
			cleanup = 1;
			oldft = fdt->ft_ptr[fd2];
		}

		ft = fdt->ft_ptr[fd];			//Fetch the pointer from ft
		fdt->ft_ptr[fd2] = ft;			//And give it to fd2
		fdt->in_use |= (1 << fd2);		//Mark in use

		filetable_incref(ft);
	spinlock_release(&fdt->t_lock);
	/* fdt op complete, safe to decref */
	if(cleanup)
		filetable_decref(oldft);
	return 0;
}

int fdtable_getentry(struct fdtable *fdt, struct filetable **ret, int fd){
	if (fd < 0 || fd >= FDT_SIZE)
		return EBADF;
	//While we're holding the t_lock, nothing can deref the ft
	spinlock_acquire(&fdt->t_lock);
		if (fdt->in_use & (1 << fd)){
			*ret = fdt->ft_ptr[fd];
		//So it's safe to try and access the ret ft
			filetable_incref(*ret);
		}
		else{		//Getting entry from invalid fd!
			spinlock_release(&fdt->t_lock);
			return EBADF;
		}
	spinlock_release(&fdt->t_lock);
	return 0;
}

int filetable_decref(struct filetable *ft){
//Lock the count so we can modify it safely
	spinlock_acquire(&ft->c_lock);
		KASSERT(ft->ref_c > 0);
		if (ft->ref_c == 1){
			filetable_destroy(ft);
			return 0;
		}
		else
			ft->ref_c--;
	spinlock_release(&ft->c_lock);
	return 0;
}

void filetable_incref(struct filetable *ft){
//Lock the count so we can modify it safely
	spinlock_acquire(&ft->c_lock);
		ft->ref_c++;
	spinlock_release(&ft->c_lock);
}

int filetable_read(struct filetable *ft, void *buf, size_t buflen, uint64_t *retval){
	int result;
	struct uio u;
	struct iovec iov;

	//Not allowed!
	if ((ft->flags & O_ACCMODE) != O_RDONLY && (ft->flags & O_ACCMODE) != O_RDWR){
		return EBADF;
	}

	//Atomic reading from the file table entry. I can't promise r/w to same file using diff ft entries
	lock_acquire(ft->rw_lock);
		//init an uio for reading operation
		filetable_uio_init(&iov, &u, buf, buflen, ft->offset, UIO_READ);
		result = VOP_READ(ft->vn, &u);
		if (result){
			lock_release(ft->rw_lock);
			return result;
		}

		ft->offset = u.uio_offset;	//Update the offset
	lock_release(ft->rw_lock);
	*retval = buflen - u.uio_resid;
	return 0;
}

int filetable_write(struct filetable *ft, const void *buf, size_t buflen, uint64_t *retval){
	int result;
	struct uio u;
	struct iovec iov;

	if ((ft->flags & O_ACCMODE) != O_WRONLY && (ft->flags & O_ACCMODE) != O_RDWR)
		return EBADF;

	lock_acquire(ft->rw_lock);
		//init an uio for reading operation
		filetable_uio_init(&iov, &u, (void*) buf, buflen, ft->offset, UIO_WRITE);
		result = VOP_WRITE(ft->vn, &u);
		if (result){
			lock_release(ft->rw_lock);
			return result;
		}

	ft->offset = u.uio_offset;	//Update the offset
	lock_release(ft->rw_lock);
	*retval = buflen - u.uio_resid;
	return 0;
}


int filetable_lseek(struct filetable *ft, off_t pos, int whence, uint64_t *retval){
	int err = 0;
	off_t newpos;
	struct stat buf;

	if (!VOP_ISSEEKABLE(ft->vn))
		return ESPIPE;

	err = VOP_STAT(ft->vn, &buf);
	if (err)
		return err;

	//Begin of critical atomic operation
	lock_acquire(ft->rw_lock);
		switch (whence){
			case SEEK_SET:
			if (pos < 0)		
				err = EINVAL;
			else
				newpos = pos;
			break;

			case SEEK_CUR:
			if (pos > 0x7fffffffffffffff - ft->offset)	//Over flow
				err = EFBIG;
			else{
				newpos = pos + ft->offset;
				if (newpos < 0)
					err = EINVAL;
			} 
			break;

			case SEEK_END:
			if (pos > 0x7fffffffffffffff - buf.st_size)							//Over file reading limits
				err = EFBIG;
			else{
				newpos = pos + buf.st_size; 
				if (newpos < 0)	
					err = EINVAL;
			}
			break;

			default:
			err = EINVAL;
			break;
		}
	if (err){
		lock_release(ft->rw_lock);
		return err;
	}
	ft->offset = newpos;
	lock_release(ft->rw_lock);
	*retval = newpos;
	return 0;
}

/*
 *	Helper function for filetable to create an uio to userspace
 */
void filetable_uio_init(struct iovec *iov, struct uio *u,
	  void *buf, size_t len, off_t pos, enum uio_rw rw){
	iov->iov_ubase = buf;
	iov->iov_len = len;
	u->uio_iov = iov;
	u->uio_iovcnt = 1;
	u->uio_offset = pos;
	u->uio_resid = len;
	u->uio_segflg = UIO_USERISPACE;
	u->uio_rw = rw;
	u->uio_space = proc_getas();
}

void fdt_fork(struct fdtable *old, struct fdtable *new){
	int i;
	//Protects the fdt table during the copy process
	spinlock_acquire(&old->t_lock);
		//Copy the pointers over
		new->in_use = old->in_use;

		//Marking the ft entries. The entries is protected by the fdt lock and can not be deleted
		for (i = 0; i < FDT_SIZE; i++){
			if (old->in_use & (1<<i)){
				filetable_incref(old->ft_ptr[i]);
				new->ft_ptr[i] = old->ft_ptr[i];
			}
		}
	spinlock_release(&old->t_lock);
}